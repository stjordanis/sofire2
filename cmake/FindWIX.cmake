# Retrieved from https://gitlab.kitware.com/cmake/community/-/wikis/contrib/modules/FindWix
# WIX homepage has moved to https://wixtoolset.org/
# -- Bob Apthorpe (bob.apthorpe at gmail.com) 20200620
# Cleaned syntax and logic
# -- Bob Apthorpe (bob.apthorpe at gmail.com) 20201012
#
# - Try to find Windows Installer XML
# See http://wix.sourceforge.net
#
# The follwoing variables are optionally searched for defaults
#  WIX_ROOT_DIR:            Base directory of WIX2 tree to use.
#
# The following are set after configuration is done:
#  WIX_FOUND
#  WIX_ROOT_DIR
#  WIX_CANDLE
#  WIX_LIGHT
#
# 2009/02 Petr Pytelka (pyta at lightcomp.cz)
#

macro(DBG_MSG _MSG)
    # message(STATUS "${CMAKE_CURRENT_LIST_FILE}(${CMAKE_CURRENT_LIST_LINE}):\n ${_MSG}")
endmacro()

# Typical root dirs of installations, exactly one of them is used
set(WIX_POSSIBLE_ROOT_DIRS
    "${WIX_ROOT_DIR}"
    "$ENV{WIX}"
    "$ENV{WIX_ROOT_DIR}"
    "$ENV{ProgramFiles}/Windows Installer XML"
)

#DBG_MSG("DBG (WIX_POSSIBLE_ROOT_DIRS=${WIX_POSSIBLE_ROOT_DIRS}")

# Select exactly ONE WIX base directory/tree to avoid mixing different
# version headers and libs
find_path(WIX_ROOT_DIR
    NAMES
    bin/candle.exe
    bin/light.exe
    PATHS ${WIX_POSSIBLE_ROOT_DIRS}
)
DBG_MSG("WIX_ROOT_DIR=${WIX_ROOT_DIR}")

# Logic selecting required libs and headers
set(WIX_FOUND OFF)
if(IS_DIRECTORY "${WIX_ROOT_DIR}")
    if(EXISTS "${WIX_ROOT_DIR}/bin/candle.exe")
        if(EXISTS "${WIX_ROOT_DIR}/bin/light.exe")
            set(WIX_FOUND ON)
            set(WIX_CANDLE ${WIX_ROOT_DIR}/bin/candle.exe)
            set(WIX_LIGHT ${WIX_ROOT_DIR}/bin/light.exe)
            #  MESSAGE(STATUS "Windows Installer XML found.")
        endif()
    endif()
endif()

# Display help message
if(NOT WIX_FOUND)
    unset(WIX_CANDLE)
    unset(WIX_LIGHT)
    # make FIND_PACKAGE friendly
    if(WIX_FIND_QUIETLY)
        message(STATUS "Warning: Windows Installer XML was not found.")
    else()
        if(WIX_FIND_REQUIRED)
            message(FATAL_ERROR
                "Windows Installer XML required but some files not found. Please specify it's location with WIX_ROOT_DIR env. variable.")
        else()
            message(STATUS "ERROR: Windows Installer XML was not found.")
        endif()
    endif()
endif()

mark_as_advanced(
    WIX_ROOT_DIR
    WIX_CANDLE
    WIX_LIGHT
)

# Call wix compiler
#
# Parameters:
#  _sources - name of list with sources
#  _obj - name of list for target objects
macro(WIX_COMPILE _sources _objs _extra_dep)
    DBG_MSG("WIX compile: ${${_sources}}")
    foreach(_current_FILE ${${_sources}})
        get_filename_component(_tmp_FILE ${_current_FILE} ABSOLUTE)
        get_filename_component(_basename ${_tmp_FILE} NAME_WE)

        set(SOURCE_WIX_FILE ${_tmp_FILE} )
        DBG_MSG("WIX source file: ${SOURCE_WIX_FILE}")

        # Check whether source exists
        if(EXISTS ${SOURCE_WIX_FILE})
            # Do nothing
        else()
            message(FATAL_ERROR "Path not exists: ${SOURCE_WIX_FILE}")
        endif()

        set(OUTPUT_WIXOBJ ${_basename}.wixobj )

        DBG_MSG("WIX output: ${CMAKE_CURRENT_BINARY_DIR}/${OUTPUT_WIXOBJ}")
        DBG_MSG("WIX command: ${WIX_CANDLE}")

        add_custom_command(
            OUTPUT    ${CMAKE_CURRENT_BINARY_DIR}/${OUTPUT_WIXOBJ}
            COMMAND   ${WIX_CANDLE}
            ARGS      ${WIX_CANDLE_FLAGS} ${SOURCE_WIX_FILE}
            DEPENDS   ${SOURCE_WIX_FILE} ${${_extra_dep}}
            COMMENT   "Compiling ${SOURCE_WIX_FILE} -> ${OUTPUT_WIXOBJ}"
        )
        set(${_objs} ${${_objs}} ${CMAKE_CURRENT_BINARY_DIR}/${OUTPUT_WIXOBJ} )
        DBG_MSG("WIX compile output: ${${_objs}}")

    endforeach()
endmacro()

# Call wix compiler
#
# Parameters:
#  _sources - name of list with sources
#  _obj - name of list for target objects
macro(WIX_COMPILE_ALL _target _sources _extra_dep)
    DBG_MSG("WIX compile all: ${${_sources}}, dependencies: ${${_extra_dep}}")

    add_custom_command(
        OUTPUT    ${_target}
        COMMAND   ${WIX_CANDLE}
        ARGS      ${WIX_CANDLE_FLAGS} -out "${_target}" ${${_sources}}
        DEPENDS   ${${_sources}} ${${_extra_dep}}
        COMMENT   "Compiling ${${_sources}} -> ${_target}"
    )
endmacro()

# Link MSI file
#
# Parameters
#  _target - Name of target file
#  _sources - Name of list with sources
macro(WIX_LINK _target _sources _loc_files)
    DBG_MSG("WIX command: ${WIX_LIGHT}\n WIX target: ${_target} objs: ${${_sources}}")

    set(WIX_LINK_FLAGS_A "")
    # Add localization
    foreach(_current_FILE ${${_loc_files}})
        set(WIX_LINK_FLAGS_A ${WIX_LINK_FLAGS_A} -loc "${_current_FILE}")
        DBG_MSG("WIX link localization: ${_current_FILE}")
    endforeach()
    DBG_MSG("WIX link flags: ${WIX_LINK_FLAGS_A}")

    add_custom_command(
        OUTPUT    ${_target}
        COMMAND   ${WIX_LIGHT}
        ARGS      ${WIX_LINK_FLAGS_A} -out "${_target}" ${${_sources}}
        DEPENDS   ${${_sources}}
        COMMENT   "Linking ${${_sources}} -> ${_target}"
    )
endmacro()