# SOFIRE2: A Simple Sodium Pool Fire Model for Liquid Metal Cooled Reactor Containment Analysis

## Overview

This package is based on the source code published in AEC Research and
Development Report AI-AEC-1305 *SOFIRE II User Report*, issued
March 30, 1973. See [https://doi.org/10.2172/4490831]

The original code was written for FORTRAN H under (IBM) OS/360; see
[http://www.bitsavers.org/pdf/ibm/360/fortran/Y28-6642-3_FortH_PLM_Nov68.pdf]

The initial goals of this project are:

* (**DONE**, one-cell code) recover the original source code as
  published in AI-AEC-1305 to a compilable runnable state on x86_64
  hardware
* (**DONE**, one-cell code) update the source code to well-formed
  Fortran 2018,
* benchmark the revised code
  * against experimental data (**in progress**, one-cell code - issues
    reconstructing benchmark input), and
  * (**DONE**, one-cell code) against previous code revisions,
* (**DONE**, one-cell code) reconstitute a sustainable cross-platform
  build/test/packaging process based on CMake/CTest/CPack,
* (**DONE**, one-cell code) address numerical problems in the original
  code (*i.e.* loss of precision leading to "clipping"; see page C-13
  of AI-AEC-1305). See also ANL-ARC-250 *Sodium pool fire phenomena, sodium pool fire modeling in SOFIRE II, and SOFIRE II calculations for the AFR-100*,
  [https://doi.org/10.2172/1054875]

## Current Status

### One-Cell Code: sofire2_1c

* Code compiles and produces results roughly similar to the reference
  data. Comparison between original 1973 single precision code and
  modernized code (both single and double precision) is documented in
  the developers guide.
* Errors have been corrected
  * Inadvertent use of `ZF2` in calculation  of `CAPF3` and `CAPF4` has
    been fixed; see Section 5, paragraph 2 on page 17 of ANL-ARC-250
    for details. This error did not affect transient output in sample
    case (transient output was numerically identical before and after
    correction).
* One-cell code has been extensively refactored and modernized:
  * Code has been upgraded to `IMPLICIT NONE` with `intent` set on all
    subprogram arguments
  * Most constants have been extracted to modules and documented
  * Floating-point precision has been parameterized and code now uses
    double precision uniformly
  * Static arrays have been converted to dynamically-allocated list,
    allowing code to run to completion; original code stopped short due
    to overwriting end of fixed output arrays
  * Better file diagnostics allowing for graceful termination of code
    if input errors occur
  * All `COMMON` blocks and intermediate input reading routines have been
    refactored away
  * All subordinate routines moved to modules
* CMake generates test results and properly compares output files with
  references
* CPack correctly generates many archives and installation packages
  * Static archives on Windows and Linux
  * NSIS (`.exe`) and WIX (`.msi`) installers on Windows
  * DEB (`.deb`) installer on Linux
  * DragNDrop (`.dmg`) installer on OSX
  * RPM (`.rpm`) installer on Linux has not yet been configured or
    tested
* Doxygen generates PDF developers guide from source code on Windows
  and Linux
  * Difficulties generating PDF under CI (LaTeX dependencies, overhead
    of texlive install)
* Code has been modified to produce CSV-formatted output to simplify
  plotting and comparison
* "clipping" behavior in one-cell code has been analyzed and documented
  (floor temperature of top layer of concrete - node 2). Minor clipping
  occurs in single precision modernized code but double precision
  modernized code produces smoother, more physically reasonable results
  consistent with ANL-ARC-250 findings.
* Benchmark cases have been developed along with Gnuplot plot files.
  Benchmarks run but are inaccurate and it is believed the SOFIRE2
  input files are incomplete. This issue requires further research.
* The thermodynamic and transport property libraries from CEA2
  (https://www.grc.nasa.gov/www/CEAWeb/) have been incorporated into the
  code, allowing for more detailed species tracking and thermophysical
  property calculation
* Much of the original code has been restructured into classes and objects
  * Unit testing of the object model is underway using the TOAST
    framework (https://github.com/thomasms/toast),
  * The CEA2 property library code is verfied by unit tests
  * Thermodynamic and transport property data for N2, O2, Na2O, and
    Na2O2 have been verified against references (JANAF, 3rd Ed.,
    Yaws 2009). See `ut_c1_property_data` unit test for details
  * Test coverage analysis is provided by `gcov` and `lcov`, providing
    detailed reports in HTML format (currently verified to work under
    Linux)
* Diagnostic logging is provided by the `m_multilog` module of FLIBS,
  allowing detailed, persistent logging of diagnostic and warning/error
  information without interfering with traditional code output written
  to `stdout`

### Two-Cell Code: sofire2_2c

The two-cell code has not yet been addressed.

### Documentation

* Parts I-IV (Introduction, Theory, Code Operation, Experimental
  Verification of Model), Appendices B, C, E, and F and 'Nomenclature
  and Definitions' are believed complete. The Verification section only
  contains the original 1973 verification data however a new code
  comparison section has been created which is consistent with the
  results shown in ANL-ARC-250
* Documentation for the input data instructions (Appendices B and E)
  have been migrated to the current report
* Documentation for the transient output quantities (Appendices C and
  F) has been migrated to the current report
* Source code appendices A and D have been omitted. Complete source is
  available for A, not yet for D.
* Sample tabular output is omitted; elements will be moved to a
  separate 'Testing' section.

## Dependencies

### Required Tools

The combined build, documentation, test, and packaging system requires
CMake and a recent Fortran compiler (supports at least F2008), for
example `gfortran` via the GCC collection (gfortran-9 works on all
platforms). On Windows, `gfortran` and the `ninja` build system are
known to work and on Linux `gfortran` should work either with `make` or
`ninja`. OSX builds work with `gfortran` and `make` installed via Fink
on El Capitan and may work with similar dependencies installed via
`brew` on later revisions of OSX.

Unit tests are implemented using the TOAST framework
(https://github.com/thomasms/toast) which is retrieved, built, and
integrated into the unit test build process by CMake. This process
should be completely automated by CMake but it may require some manual
assistance; see the section on Build Process. In limited networking
environments where pulling source code directly from public repositories
is difficult or impossible, `./cmake/BuildTOAST.cmake` may be modified
to point at a local archive or repository instead (beware -- TOAST
makes use of git submodules to retrieve its own dependencies). While
`git` is used by CMake for retrieving the TOAST sources, `git` is not
otherwise required for building SOFIRE2.

The diagnostic logging facility and command line argument processor in
SOFIRE2 are implemented by `m_multilog.f90` and `command_args.f90` from
FLIBS (http://flibs.sourceforge.net/). While FLIBS may be retrieved
manually, the project currently uses `subversion` to retrieve the
source code from the remote repository during the build process.
As above, `./cmake/BuildFLIBS.cmake` may be modified to point at a
local archive or repository if network access or `subversion` is not
available. As with `git`, `subversion` is not otherwise required for
building or using SOFIRE2.

See [Adding FLIBS Source Code Dependencies without Network Access or Subversion](contrib/FLIBS-r415/README_SOFIRE.md)
for instructions on building SOFIRE2 using alternate sources for
`m_multilog.f90` and `command_args.f90`.

### Optional Tools

Optional developer documentation is produced using Doxygen which
expects to find `dot` from Graphviz. PDF documentation is generated via
`LaTeX`; the TeXLive distribution is known to work on both Windows and
Linux and additionally Perl is necessary to support bibliography and
index processing. The code comparison documentation relies on plots
generated via Gnuplot.

Packaging utilities vary by operating system but on Windows both WIX
and NSIS are supported. ZIP archives are supported by default on all
platforms, `.tar.gz` and `.tar.bz2` archives are supported by default
on unix-like systems (OSX, Linux). DragNDrop (`.dmg`) installers are
created on OSX systems and `.deb` packages are built on Debian-flavored
Linux systems. CPack will only attempt to build packages if the
requisite packaging utilities are detected; at a minimum, static zip
archives are supported on every platform.

`git` is used by CMake to retrieve the TOAST unit test framework and
its dependencies but otherwise is not required to build or use
SOFIRE2.

`subversion` is used by CMake to retrieve the FLIBS suite of utility
 routines but otherwise is not required to build or use SOFIRE2.

Test coverage metrics can be generated with gcov
(https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and lcov
(http://ltp.sourceforge.net/coverage/lcov.php)

## Build Process

The software build process for SOFIRE2 follows the basic CMake build
process:

* Download and unpack / pull source distribution
* `cd` *project root*
* `mkdir build`
* `cd build`
* `cmake ..` with options like `-C Release` or `Debug`,
  `-G` *generator*, `-D` *config vars*
* build with `make`, `ninja`, *etc.* (depends on platform;
  see **Required Tools**).

### Building With `make`:

  * `make`
  * `make test`
  * `make docs` *optional*
  * `make package` *optional*

### Building With `ninja`:

  * `ninja`
  * `ninja test`
  * `ninja docs` *optional*
  * `ninja package` *optional*

Packaging with `make package` is strongly encouraged to allow easy
distribution, installation, and removal

### Continuous Integration

SOFIRE2 is built, tested, and packaged using a continuous integration
(CI) pipeline on Gitlab. Each change committed to the repository is
evaluated in a Windows environment and two different Linux environments
(Ubuntu and Fedora). The build, test, and package jobs described
previously are implemented in each environment and the
[definition of the CI pipeline](.gitlab-ci.yml) is stored in the project
repository as `.gitlab-ci.yml`. This file is particularly instructive
for setting up Windows development and test environments.

## Code of Conduct

Treat others with respect and dignity. For more information, refer to
this project's [Code of Conduct](code_of_conduct.md)

## License

The original code was produced by the Atomics International Division of
Rockwell International for the US Atomic Energy Commission. No license was
provided for the original 1973 code as was the practice at the time. For
the purpose of furthering education and research and to preserve the legacy
of this software, this project is released under the MIT License
(see LICENSE). It is not clear what the license should be but it is felt
that the MIT License is appropriate here.

Thermodynamic and transport data library files are taken from `CEA2`,
`CAP`, `PAC` (https://www.grc.nasa.gov/www/CEAWeb/topicsHome.htm).
These data are compiled from public sources and while it is not clear
which (if any) license NASA releases these files under, the data has
been published in public NASA technical documents since the 1960s. For
example, see tables C2. and E.2 in NASA Reference Publication 1311,
*Computer Program for Calculation of Complex Chemical Equilibrium Compositions and Applications*,
Volume II: *Users Manual and Program Description*, June 1996. There
appears to be no restriction on the distribution of this data or nor is
there and obvious copyright or license provided so these files are
treated as public data.

Portions of the `CMake-codecov` project are included, originally from
https://github.com/RWTH-HPC/CMake-codecov. The files
`cmake/Findcodecov.cmake`, `cmake/FindGcov.cmake`,
`cmake/FindLcov.cmake` and `cmake/llvm-cov-wrapper` are released under
a BSD 3-Clause License (see
https://github.com/RWTH-HPC/CMake-codecov/blob/master/LICENSE)

Portions of the FLIBS set of utility routines are included in this
distribution; see [contrib/FLIBS-r415](contrib/FLIBS-r415) for more
information including license, essentially a BSD-style license
compatible with this project.

## Image Credits

Uses icons from "Project Icons" by Mihaiciuc Bogdan.
[http://bogo-d.deviantart.com]
Icon set license: CC Attribution 4.0
[https://iconarchive.com/icons/bogo-d/project/License.pdf]

## Special Thanks

Thanks to Lars Viklund for the contributed EasyBuild configuration