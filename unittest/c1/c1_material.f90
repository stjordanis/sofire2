!> @file c1_material.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_material module. Some reference
!! thermodynamic data taken from @cite Haberman1980, some reference
!! transport correlations taken from @cite Yaws2009
program c1_material
    use sofire2_param, only: WP
    use m_constant, only: ZERO, HALF, ONE, TWO, JOULE_BTU, LBM_G, T0K,  &
        RGAS
    use m_material
    use toast
    implicit none

    character(len=6), dimension(*), parameter :: test_gas_name = (/     &
        'Ar    ', 'CH4   ', 'CO    ', 'CO2   ', 'H2    ', 'H2O   ',     &
        'He    ', 'Kr    ', 'N2    ', 'Na    ', 'Ne    ', 'O2    ',     &
        'Xe    ', 'ar    ', 'AR    ', '    Ar', 'ArArAr', '  O 2 ',     &
        '      ', ' H2 O ', '  Na  ', 'Na2O2 ' /)

    integer, dimension(*), parameter :: test_gas_id = (/                &
        gid_Ar,   gid_CH4,  gid_CO,   gid_CO2, gid_H2,   gid_H2O,       &
        gid_He,   gid_Kr,   gid_N2,   gid_Na,  gid_Ne,   gid_O2,        &
        gid_Xe,   gid_NULL, gid_NULL, gid_Ar,  gid_NULL, gid_NULL,      &
        gid_NULL, gid_NULL, gid_Na,   gid_NULL /)

    character(len=6), dimension(*), parameter :: test_aer_name = (/     &
        'H2O   ', 'Na    ', 'NaO2  ', 'NaOH  ', 'Na2CO3', 'Na2O  ',     &
        'Na2O2 ', '      ', ' H2 O ', '  Na  ', 'Xe    ' /)

    integer, dimension(*), parameter :: test_aer_id = (/                &
        aid_H2O,   aid_Na,   aid_NaO2, aid_NaOH, aid_Na2CO3, aid_Na2O,  &
        aid_Na2O2, aid_NULL, aid_NULL, aid_Na,   aid_NULL  /)

    real(kind=WP), dimension(*), parameter :: cp_h20_hj = (/            &
        1.851_WP, 1.852_WP, 1.869_WP, 1.890_WP, 1.913_WP,               &
        1.939_WP, 1.967_WP, 1.997_WP, 2.029_WP, 2.061_WP,               &
        2.095_WP, 2.129_WP, 2.164_WP, 2.198_WP, 2.233_WP,               &
        2.266_WP, 2.299_WP, 2.331_WP, 2.369_WP, 2.407_WP,               &
        2.440_WP, 2.473_WP, 2.504_WP, 2.535_WP, 2.565_WP,               &
        2.593_WP, 2.621_WP, 2.648_WP, 2.673_WP, 2.698_WP,               &
        2.721_WP, 2.744_WP /)

    real(kind=WP), dimension(*), parameter :: cp_co2_hj = (/            &
        0.761_WP, 0.817_WP, 0.869_WP, 0.916_WP, 0.958_WP,               &
        0.995_WP, 1.029_WP, 1.060_WP, 1.088_WP, 1.113_WP,               &
        1.137_WP, 1.158_WP, 1.177_WP, 1.195_WP, 1.212_WP,               &
        1.227_WP, 1.240_WP, 1.253_WP, 1.264_WP, 1.275_WP,               &
        1.285_WP, 1.294_WP, 1.302_WP, 1.309_WP, 1.317_WP,               &
        1.323_WP, 1.329_WP, 1.335_WP, 1.340_WP, 1.345_WP,               &
        1.350_WP, 1.355_WP /)

    ! Note: Entry for 100 C (69.4) was calculated by linear
    ! interpolation from https://www.ohio.edu/mechanical/thermo/property_tables/combustion/oxygen_enth.html
    ! Original tabular entry was 63.5, yielding a suspiciously large
    ! deviation against CEA2 (9%). Remaining values are within 2%
    real(kind=WP), dimension(*), parameter :: h_o2_hj = (/              &
         -69.3_WP,  -22.9_WP,   23.0_WP,   69.4_WP,  116.4_WP,          &
         164.2_WP,  212.7_WP,  262.1_WP,  312.2_WP,  363.0_WP,          &
         414.6_WP,  466.7_WP,  519.4_WP,  572.6_WP,  626.3_WP,          &
         680.4_WP,  734.7_WP,  789.7_WP,  844.9_WP,  900.3_WP,          &
         956.1_WP, 1012.1_WP /)

    ! Entropy in J/mol-K from JANAF, from 200 K to 900 K in 100 K
    ! increments. 900 K was selected as the upper cutoff because
    ! JANAF data assumes alpha-phase solid above 948 K while CEA2
    ! assumes liquid phase, resulting in ~10% deviation in entropy
    ! values
    real(kind=WP), dimension(*), parameter :: s_na2o2_j = (/            &
         61.785_WP,  95.354_WP, 122.268_WP, 144.748_WP, 164.093_WP,     &
        181.108_WP, 203.601_WP, 216.981_WP /)

    type(material_database) :: mdb

    real(kind=WP) :: T
    real(kind=WP) :: cp_offset
    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    call test%init(name="c1_material")

    ! get_gas_id

    do i = 1, size(test_gas_name)
        id = get_gas_id(test_gas_name(i))
        call test%asserttrue((id == test_gas_id(i)),                    &
            message="Matching gas id with [" // test_gas_name(i) // "]")
    end do

    ! get_aerosol_id

    do i = 1, size(test_aer_name)
        id = get_aerosol_id(test_aer_name(i))
        call test%asserttrue((id == test_aer_id(i)),                    &
            message="Matching aerosol id with [" // test_aer_name(i)    &
            // "]")
    end do

    ! is_gas_id
    do i = -2, 15
        if (i < 1 .or. i > 13) then
            call test%asserttrue(.not. is_gas_id(i),                    &
                message="Invalid gas id")
        else
            call test%asserttrue(is_gas_id(i),                          &
                message="Valid gas id")
        end if
    end do

    ! is_aerosol_id
    do i = -4, 9
        if (i < 1 .or. i > 7) then
            call test%asserttrue(.not. is_aerosol_id(i),                &
                message="Invalid aerosol id")
        else
            call test%asserttrue(is_aerosol_id(i),                      &
                message="Valid aerosol id")
        end if
    end do

    ! material_database class

    call mdb%init()

    ! Verify all entries are empty
    call test%asserttrue(all(mdb%gas(:)%id == gid_NULL),                &
        message="Every mdb gas ID is gid_NULL")
    call test%asserttrue(all(mdb%gas(:)%mw == ZERO),                    &
        message="Every mdb gas mw is zero")
    call test%asserttrue(all(mdb%gas(:)%dHform == ZERO),                &
        message="Every mdb gas dHform is zero")
    call test%asserttrue(all(mdb%gas(:)%h_std_0 == ZERO),               &
        message="Every mdb gas h_std_0 is zero")
    call test%asserttrue(all(mdb%gas(:)%phase_id == phase_NULL),        &
        message="Every mdb gas phase_id is zero")
    call test%asserttrue(all(mdb%gas(:)%n_cp < 0),                      &
        message="Every mdb gas n_cp is zero")
    call test%asserttrue(all(mdb%gas(:)%n_k < 0),                       &
        message="Every mdb gas n_k is zero")
    call test%asserttrue(all(mdb%gas(:)%n_mu < 0),                      &
        message="Every mdb gas n_mu is zero")

    call test%asserttrue(all(mdb%aerosol(:)%id == aid_NULL),            &
        message="Every mdb aerosol ID is aid_NULL")
    call test%asserttrue(all(mdb%aerosol(:)%mw == ZERO),                &
        message="Every mdb aerosol mw is zero")
    call test%asserttrue(all(mdb%aerosol(:)%dHform == ZERO),            &
        message="Every mdb aerosol dHform is zero")
    call test%asserttrue(all(mdb%aerosol(:)%h_std_0 == ZERO),           &
        message="Every mdb aerosol h_std_0 is zero")
    call test%asserttrue(all(mdb%aerosol(:)%phase_id == phase_NULL),    &
        message="Every mdb aerosol phase_id is phase_NULL")
    call test%asserttrue(all(mdb%aerosol(:)%n_cp < 0),                  &
        message="Every mdb aerosol n_cp is zero")

    call mdb%populate()

    ! General temperature-independent gas property reasonableness
    ! checking
    do i = 1, n_gas
        call test%asserttrue(mdb%gas(i)%id == i,                        &
            message="Every mdb gas ID matches its index")
    end do
    call test%asserttrue(all(mdb%gas(:)%id >= 1)                        &
        .and. all(mdb%gas(:)%id <= n_gas),                              &
        message="Every mdb gas ID is in range")
    call test%asserttrue(all(mdb%gas(:)%mw > HALF)                      &
        .and. all(mdb%gas(:)%mw < 250.0_WP),                            &
        message="Every mdb gas mw is reasonable")
    ! call test%asserttrue(all(mdb%gas(:)%dHform == ZERO),                &
    !     message="Every mdb gas dHform is zero")
    ! call test%asserttrue(all(mdb%gas(:)%h_std_0 == ZERO),               &
    !     message="Every mdb gas h_std_0 is zero")
    call test%asserttrue(all(mdb%gas(:)%phase_id == phase_GAS),         &
        message="Every mdb gas phase_id is phase_GAS")
    call test%asserttrue(all(mdb%gas(:)%n_cp >= 0),                     &
        message="Every mdb gas n_cp is at least zero")
    call test%asserttrue(all(mdb%gas(:)%n_k >= 0),                      &
        message="Every mdb gas n_k is at least zero")
    call test%asserttrue(all(mdb%gas(:)%n_mu >= 0),                     &
        message="Every mdb gas n_mu is at least zero")

    ! General temperature-independent aerosol property reasonableness
    ! checking
    do i = 1, n_aer
        call test%asserttrue(mdb%aerosol(i)%id == i,                    &
            message="Every mdb aerosol ID matches its index")
    end do
    call test%asserttrue(all(mdb%aerosol(:)%id >= 1)                    &
        .and. all(mdb%aerosol(:)%id <= n_aer),                          &
        message="Every mdb aerosol ID is in range")
    call test%asserttrue(all(mdb%aerosol(:)%mw > 18.0_WP)               &
        .and. all(mdb%aerosol(:)%mw < 110.0_WP),                        &
        message="Every mdb aerosol mw is reasonable")
    ! call test%asserttrue(all(mdb%aerosol(:)%dHform == ZERO),           &
    !     message="Every mdb aerosol dHform is zero")
    ! call test%asserttrue(all(mdb%aerosol(:)%h_std_0 == ZERO),          &
    !     message="Every mdb aerosol h_std_0 is zero")
    call test%asserttrue(                                               &
        all(mdb%aerosol(:)%phase_id >= phase_CONDENSED),                &
        message="Every mdb aerosol phase_id is phase_CONDENSED")
    call test%asserttrue(all(mdb%aerosol(:)%n_cp >= 0),                 &
        message="Every mdb aerosol n_cp is at least zero")

    ! for mdb%gas and mdb%aerosol
    ! - spot-check id
    ! - spot-check name
    ! - spot-check mw
    ! - spot-check dHform
    ! - spot-check h_std_0
    ! - spot-check phase_id

    ! Check O2, N2, Na2O, Na2O2 properties (cp for all, mu & k for gas)

    call test%assertequal(mdb%gas(gid_Ar)%dHform, ZERO,                 &
        message="hForm(Ar(g)) should be zero")

    call test%assertequal(mdb%gas(gid_H2)%dHform, ZERO,                 &
        message="hForm(H2(g)) should be zero")

    call test%assertequal(mdb%gas(gid_He)%dHform, ZERO,                 &
        message="hForm(He(g)) should be zero")

    call test%assertequal(mdb%gas(gid_Kr)%dHform, ZERO,                 &
        message="hForm(Kr(g)) should be zero")

    call test%assertequal(mdb%gas(gid_N2)%dHform, ZERO,                 &
        message="hForm(N2(g)) should be zero")

    call test%assertequal(mdb%gas(gid_Ne)%dHform, ZERO,                 &
        message="hForm(Ne(g)) should be zero")

    call test%assertequal(mdb%gas(gid_O2)%dHform, ZERO,                 &
        message="hForm(O2(g)) should be zero")

    call test%assertequal(mdb%gas(gid_Xe)%dHform, ZERO,                 &
        message="hForm(Xe(g)) should be zero")

    ! Cp(H2O(g)) from -50C to 1500C, App. B, Table B-15
    ! "Engineering Thermodynamics" Haberman, William and John, James,
    ! 1980, Allyn and Bacon
    T = -100.0_WP
    cp_offset = RGAS / mdb%gas(gid_H2O)%mw
    do i = 1, size(cp_h20_hj)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'Cp(H2O)/Cp_ref at ', T,    &
        !      ' C = ', mdb%gas(gid_H2O)%cp(T + T0K) * cp_offset / cp_h20_hj(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_H2O)%cp(T + T0K) * cp_offset, &
            cp_h20_hj(i), rel_tol=0.011_WP,                             &
            message="Cp(H2O(g)) +-1.1% kJ/kg-K")
    end do

    call test%asserttrue(mdb%gas(gid_H2O)%matches_name(                 &
        "H2O                             "),                            &
        message="'H2O' matches name of gas(gid_H2O)")

    call test%asserttrue(.not. mdb%gas(gid_H2O)%matches_name(           &
        "H2                              "),                            &
        message="'H2' does not match name of gas(gid_H2O)")

    call test%assertequal(mdb%id_from_gas_name(                         &
        "H2O                             "), gid_H2O,                   &
        message="Matched gas ID gid_H2O for 'H2O'")

    ! Cp(CO2(g)) from -50C to 1500C, App. B, Table B-15
    ! "Engineering Thermodynamics" Haberman, William and John, James,
    ! 1980, Allyn and Bacon
    T = -100.0_WP
    cp_offset = RGAS / mdb%gas(gid_CO2)%mw
    do i = 1, size(cp_co2_hj)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'Cp(CO2)/Cp_ref at ', T,    &
        !      ' C = ', mdb%gas(gid_CO2)%cp(T + T0K) * cp_offset / cp_co2_hj(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_CO2)%cp(T + T0K) * cp_offset, &
            cp_co2_hj(i), rel_tol=0.005_WP,                             &
            message="Cp(CO2(g)) +-0.5% kJ/kg-K")
    end do

    ! h(O2(g)) from -50C to 1000C, App. B, Table B-17
    ! "Engineering Thermodynamics" Haberman, William and John, James,
    ! 1980, Allyn and Bacon. Note that h(100C) was interpolated as
    ! 69.4 kJ/kg from a different source (see above) vs 63.5 kJ/kg
    ! as given in the table. Lower value gave ~9% deviation with CEA2
    ! correlation; remaining values are within 3%
    T = -100.0_WP
    cp_offset = RGAS / mdb%gas(gid_O2)%mw
    do i = 1, size(h_o2_hj)
        T = T + 50.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'h(O2)/h_ref at ', T, &
        !      ' C = ', mdb%gas(gid_O2)%h(T + T0K) * cp_offset / h_o2_hj(i), ''!' kJ/kg-K'
        call test%assertequal(mdb%gas(gid_O2)%h(T + T0K) * cp_offset,   &
            h_o2_hj(i), rel_tol=0.03_WP,                                &
            message="h(O2(g)) +-3.0% kJ/kg")
    end do
    ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'h(O2) at ', 25.0_WP,    &
    !     ' C = ', mdb%gas(gid_O2)%h(25.0_WP + T0K) * cp_offset, ' kJ/kg-K'
    call test%assertequal(mdb%gas(gid_O2)%h(25.0_WP + T0K) * cp_offset, &
        ZERO, abs_tol=0.05_WP, message="h(O2(g)) at 25C should be zero")

    ! S(Na2O2(condensed)) from 200K to 900K, Chase 1985 (JANAF, 3rd Ed.)
    T = 100.0_WP
    cp_offset = RGAS
    do i = 1, size(s_na2o2_j)
        T = T + 100.0_WP
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'S(Na2O2)/S_ref at ', T, &
        !      ' C = ', mdb%aerosol(aid_Na2O2)%s(T) * cp_offset / s_na2o2_j(i), ''!' kJ/kg-K'
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ', T,   &
        !      ' K = S(Na2O2) = ', mdb%aerosol(aid_Na2O2)%s(T) * cp_offset,   &
        !      ' vs S_ref = ', s_na2o2_j(i), ' kJ/kmol-K'
        call test%assertequal(mdb%aerosol(aid_Na2O2)%s(T) * cp_offset,  &
            s_na2o2_j(i), rel_tol=0.01_WP,                              &
            message="S(Na2O2(condensed)) +-1% kJ/kmol (<1000K)")
    end do

    call test%asserttrue(mdb%aerosol(aid_Na2O2)%matches_name(           &
        "Na2O2                           "),                            &
        message="'Na2O2' matches name of aerosol(aid_Na2O2)")

    call test%asserttrue(.not. mdb%aerosol(aid_Na2O2)%matches_name(     &
        "STANK                           "),                            &
        message="'STANK' does not match name of aerosol(aid_Na2O2)")

    call test%assertequal(mdb%id_from_aerosol_name(                     &
        "Na2O2                           "), aid_Na2O2,                 &
        message="Matched aerosol ID aid_Na2O2 for 'Na2O2'")

    call test%assertequal(mdb%aerosol(aid_Na2O2)%cp_t_lo(), 200.0_WP,   &
        rel_tol=0.01_WP, message="cp(Na2O2(condensed)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%aerosol(aid_Na2O2)%cp_t_hi(), 6000.0_WP,  &
        rel_tol=0.01_WP, message="cp(Na2O2(condensed)) T-upper = 6000 K +-1%")

    T = mdb%aerosol(aid_Na2O2)%cp_t_lo() - ONE
    call test%asserttrue(mdb%aerosol(aid_Na2O2)%cp(T) < ZERO,           &
        message="cp(Na2O2(condensed)) below T-lower is negative (-404)")

    T = mdb%aerosol(aid_Na2O2)%cp_t_hi() + ONE
    call test%asserttrue(mdb%aerosol(aid_Na2O2)%cp(T) < ZERO,           &
        message="cp(Na2O2(condensed)) above T-upper is negative (-404)")

    ! mu(N2(g)) from 250K to 1900K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    T = 200.0_WP
    cp_offset = 1.0_WP ! RGAS
    do i = 1, 35
        T = T + 50.0_WP
        refval = 4.4656 + T * (6.3814E-1_WP                             &
            + T * (- 2.6956E-4 + T * 5.4113E-8))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'mu(N2)/mu_ref at ', &
        !     T, ' K = ', mdb%gas(gid_N2)%mu(T) / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_N2)%mu(T),                 &
        !     ' vs mu_ref = ', refval, ' micropoise'
        call test%assertequal(mdb%gas(gid_N2)%mu(T),                    &
            refval, rel_tol=0.05_WP,                                    &
            message="mu(N2(g)) +-5%")
    end do

    call test%assertequal(mdb%gas(gid_N2)%mu_t_lo(), 200.0_WP,          &
        rel_tol=0.01_WP, message="mu(N2(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_N2)%mu_t_hi(), 15000.0_WP,        &
        rel_tol=0.01_WP, message="mu(N2(g)) T-upper = 15000 K +-1%")

    T = mdb%gas(gid_N2)%mu_t_lo() - ONE
    call test%asserttrue(mdb%gas(gid_N2)%mu(T) < ZERO,                  &
        message="mu(N2(g)) below T-lower is negative (-404)")

    T = mdb%gas(gid_N2)%mu_t_hi() + ONE
    call test%asserttrue(mdb%gas(gid_N2)%mu(T) < ZERO,                  &
        message="mu(N2(g)) above T-upper is negative (-404)")

    ! k(O2(g)) from 200K to 2000K, Yaws 2009
    ! (Transport Properties of Hydrocarbons and Chemicals)
    ! Note that CEA2 returns k as W/cm-K * 1E6 or W/m-K * 1E4. That is,
    ! divide k(CEA2) by 10000 to get conductivity in units of W/m-K
    T = 150.0_WP
    cp_offset = 1.0E-4_WP
    do i = 1, 37
        T = T + 50.0_WP
        refval = 1.5475E-4 + T * (9.4153E-5_WP                          &
            + T * (-2.7529E-8 + T * 5.2069E-12))
        ! write(unit=6, fmt='(A, F7.1, A, F6.3, A)') 'k(O2)/k_ref at ',   &
        !     T, ' K = ', mdb%gas(gid_O2)%k(T) * cp_offset / refval, ''
        ! write(unit=6, fmt='(A, F7.1, A, ES12.4, A, ES12.4, A)') 'At ',  &
        !     T, ' K = mu(N2) = ', mdb%gas(gid_O2)%k(T) * cp_offset,      &
        !     ' vs mu_ref = ', refval, ' W/m-K'
        call test%assertequal(mdb%gas(gid_O2)%k(T) * cp_offset,         &
            refval, rel_tol=0.03_WP,                                    &
            message="k(O2(g)) +-3%")
    end do

    call test%assertequal(mdb%gas(gid_O2)%k_t_lo(), 200.0_WP,           &
        rel_tol=0.01_WP, message="k(O2(g)) T-lower = 200 K +-1%")

    call test%assertequal(mdb%gas(gid_O2)%k_t_hi(), 15000.0_WP,         &
        rel_tol=0.01_WP, message="k(O2(g)) T-upper = 15000 K +-1%")

    T = mdb%gas(gid_O2)%k_t_lo() - ONE
    call test%asserttrue(mdb%gas(gid_O2)%k(T) < ZERO,                   &
        message="k(O2(g)) below T-lower is negative (-404)")

    T = mdb%gas(gid_O2)%k_t_hi() + ONE
    call test%asserttrue(mdb%gas(gid_O2)%k(T) < ZERO,                   &
        message="k(O2(g)) above T-upper is negative (-404)")

    ! *** Heat of formation ***

    ! Heat released per pound of Na consumed for Na2O: 3876.4 vs 3900.0
    call test%assertequal(-mdb%aerosol(aid_Na2O)%dHform                 &
        / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G),           &
        3900.0_WP, rel_tol=0.01_WP,                                     &
        message="hForm(Na2O)/mNa is about 3900 +-1% BTU/lbm-Na")
    ! write(unit=6, fmt="(A, F12.4)") "hForm(Na2O)/mNa is about 3900 BTU/lbm-Na vs ", &
    !     -mdb%aerosol(aid_Na2O)%dHform                                   &
    !     / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G)
    call test%assertequal(mdb%aerosol(aid_Na2O)%dHform, -417.98E3_WP,   &
        abs_tol=4.2E3_WP,                                               &
        message="hForm(Na2O) is -417.98kJ/mol (JANAF)")
    ! write(unit=6, fmt="(A, ES16.6)") "hForm(Na2O) expected to be -417.98kJ/mol vs ", &
    !     mdb%aerosol(aid_Na2O)%dHform

    ! Heat released per pound of Na consumed for Na2O2: 4787.4 vs 4500.0
    ! Note that JANAF also gives about 4800 BTU/lbm-Na; suspect SOFIRE II data is low
    call test%assertequal(-mdb%aerosol(aid_Na2O2)%dHform                &
        / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G),           &
        4500.0_WP, rel_tol=0.08_WP,                                     &
        message="hForm(Na2O2)/mNa is 4500 +-8% BTU/lbm-Na")
    ! write(unit=6, fmt="(A, F12.4)") "hForm(Na2O2)/mNa is about 4500 BTU/lbm-Na vs ", &
    !     -mdb%aerosol(aid_Na2O2)%dHform                                  &
    !     / (TWO * mdb%aerosol(aid_Na)%mw * JOULE_BTU * LBM_G)
    call test%assertequal(mdb%aerosol(aid_Na2O2)%dHform, -513.21E3_WP,  &
        abs_tol=5.0E3_WP,                                               &
        message="hForm(Na2O2) is -513.21kJ/mol (JANAF)")
    ! write(unit=6, fmt="(A, ES16.6)") "hForm(Na2O2) expected to be -513.2kJ/mol vs ", &
    !     mdb%aerosol(aid_Na2O2)%dHform

    ! ! Print summary and reset
    ! call printsummary(test)
    ! call test%reset()

    ! ! logical asserts
    ! call test%asserttrue(.true., message = "True should be true.")
    ! call test%asserttrue(.false., "False should not be true.")
    ! call test%asserttrue(2_ki4*3_ki4 == 6_ki4, "2*3 == 6 should be true.")
    ! call test%assertfalse(.false., "False should be false.")
    ! call test%assertequal("string one", "string two", "Assert strings are not equal")
    ! call test%assertequal("string one", "string one", "Assert strings are now equal")

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_material.json")

    call test%checkfailure()
end program c1_material
