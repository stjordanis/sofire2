!> @file c1_burnchem.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_burnchem module.
program c1_burnchem
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_burnchem
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_burnchem")

    !!! Test exposed functions

    ! k_na2o = k_from_s(S)

    ! S = s_from_kna2o(k_monoxide)

    ! k = k_from_yna2o(y_monoxide)

    ! y_na2o = yna2o_from_k(k)

    ! qburn_mNa = qburn_mNa_from_k(k_monoxide, use_legacy)

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_burnchem.json")

    call test%checkfailure()
end program c1_burnchem
