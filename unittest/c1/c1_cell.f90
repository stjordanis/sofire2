!> @file c1_cell.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_cell module.
program c1_cell
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_cell
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_cell")

    !!! Test classes

    ! !> Gas space control volume
    ! type, public :: cell
    !     !> Identifying name
    !     character(len=32) :: name = 'UNINITIALIZED CELL             '
    !     !> Pressure, \f$\us{\lbf\per\square\foot}\f$
    !     real(kind=WP) :: p = ZERO
    !     !> Volume, \f$\us{\cubic\foot}\f$
    !     real(kind=WP) :: v = ZERO
    !     !> Temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: t = ZERO
    !     !> Gas inventory
    !     type(inventory) :: gas
    !     !> Aerosol inventory
    !     type(inventory) :: aerosol
    ! contains
    !     !> Initialize cell attributes
    !     call cell%init()
    !     !> Set gas species masses based on ideal gas law and mass
    !     !! distribution `mf`
    !     call cell%set_ideal_gas_mass()
    !     !> Return combined mass of gas and aerosol in cell,
    !     !! \f$\us{\lbm}\f$
    !     m = cell%mass()
    !     !> Return aggregate gas-space density (both gas and aerosol
    !     !! mass), \f$\us{\lbm\per\cubic\foot}\f$
    !     rhoc = cell%mixture_density()
    !     !> Return density of cell gas (does not include aerosol mass),
    !     !! \f$\us{\lbm\per\cubic\foot}\f$
    !     rhog = cell%gas_density()
    !     !> Return density of cell aerosol (does not include gas mass),
    !     !! \f$\us{\lbm\per\cubic\foot}\f$
    !     rhog = cell%aerosol_density()
    !     !> Mass fraction of oxygen in the cell
    !     mf_o2 = cell%mf_o2()
    ! end type cell

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_cell.json")

    call test%checkfailure()
end program c1_cell
