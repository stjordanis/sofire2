!> @file c1_dataframe.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Unit tests for m_dataframe module.
program c1_dataframe
    use sofire2_param, only: WP
    use m_constant, only: ZERO!, HALF, TWO, JOULE_BTU, LBM_G, T0K, RGAS
    use m_dataframe
    use toast
    implicit none

    real(kind=WP) :: refval
    integer :: i
    integer :: id
    type(TestCase) :: test

    continue

    call test%init(name="c1_dataframe")

    !!! Test classes

    ! ----- result_dataframe

    ! type, public :: result_dataframe
    !     ! Original tracked transient elements, COMMON /COMA/
    !     !> Scenario time, hours
    !     real(kind=WP) :: T = ZERO
    !     !> Mass of sodium oxide released to gas, \f$\us{\lbm}\f$
    !     real(kind=WP) :: OXR = ZERO
    !     !> Cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
    !     real(kind=WP) :: PGASC = ZERO
    !     !> Liner floor temperature, center of first (top) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF1 = ZERO
    !     !> Cell floor temperature, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF2 = ZERO
    !     !> Cell floor temperature, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF3 = ZERO
    !     !> Cell floor temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF4 = ZERO
    !     !> Cell gas temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TGASC = ZERO
    !     !> Sodium surface temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS = ZERO
    !     !> Sodium pool temperature, center of first (top) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS1 = ZERO
    !     !> Sodium pool temperature, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS2 = ZERO
    !     !> Sodium pool temperature, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS3 = ZERO
    !     !> Sodium pool temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS4 = ZERO
    !     !> Liner wall temperature, center of first (inside) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC1 = ZERO
    !     !> Cell wall temperature, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC2 = ZERO
    !     !> Cell wall temperature, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC3 = ZERO
    !     !> Cell wall temperature, center of fourth (outside) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC4 = ZERO
    !     !> Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
    !     real(kind=WP) :: XM = ZERO

    !     ! Previously untracked transient elements
    !     !> Convective heat transfer rate from sodium to gas, \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QCONV1 = ZERO
    !     !> Convective heat transfer rate from gas to wall liner (node 1), \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QCONV2 = ZERO
    !     !> Radiative heat transfer rate from wall liner (node 1) to node 2, \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QRAD1 = ZERO
    !     !> Radiative heat transfer rate from floor liner (node 1) to node 2, \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QRAD2 = ZERO
    !     !> Radiative heat transfer rate from sodium pool to gas, \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QRAD3 = ZERO
    !     !> Radiative heat transfer rate from sodium pool to wall liner (node 1), \f$\us{\BTU\per\hour}\f$
    !     real(kind=WP) :: QROD = ZERO
    !     !> Mass of oxygen remaining in cell, \f$\us{\lbm}\f$
    !     real(kind=WP) :: OX = ZERO
    !     !> Mass of oxygen consumed by combustion, \f$\us{\lbm}\f$
    !     real(kind=WP) :: OXLB = ZERO
    !     !> Gas density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHOC = ZERO
    !     !> Mass of oxygen consumed, \f$\us{\lbm}\f$
    !     real(kind=WP) :: SUM1 = ZERO
    !     !> Mass of sodium burned, \f$\us{\lbm}\f$
    !     real(kind=WP) :: SUM2 = ZERO
    !     !> Mass of gas in cell, \f$\us{\lbm}\f$
    !     real(kind=WP) :: W = ZERO
    !     !> Volumetric gas leakage rate, \f$\us{\cubic\foot\per\hour}\f$
    !     real(kind=WP) :: F40 = ZERO
    !     !> Oxygen concentration corrected for leakage, weight-percent
    !     real(kind=WP) :: C = ZERO
    !     !> Gas leakage from cell due to exhaust, \f$\us{\lbm}\f$
    !     real(kind=WP) :: W1 = ZERO
    !     !> Gas inflow/outflow mass from cell due to pressure, \f$\us{\lbm}\f$
    !     real(kind=WP) :: W2 = ZERO
    !     !> Timestep based on gas mass, \f$\si{\hour}\f$
    !     real(kind=WP) :: DTMIN1 = ZERO
    !     !> Timestep based on sodium pool surface, \f$\si{\hour}\f$
    !     real(kind=WP) :: DTMIN2 = ZERO
    !     !> Minimum timestep, \f$\si{\hour}\f$
    !     real(kind=WP) :: DT = ZERO
    !     !> Stabilized timestep, \f$\si{\hour}\f$
    !     real(kind=WP) :: DTMIN = ZERO

    !     !> Floor liner temperature change over timestep, center of first (top) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTF1 = ZERO
    !     !> Cell floor temperature change over timestep, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTF2 = ZERO
    !     !> Cell floor temperature change over timestep, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTF3 = ZERO
    !     !> Cell floor temperature change over timestep, center of fourth (bottom) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTF4 = ZERO
    !     !> Cell gas temperature change over timestep, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTGASC = ZERO
    !     !> Sodium surface temperature change over timestep, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTS = ZERO
    !     !> Sodium pool temperature change over timestep, center of first (top) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTS1 = ZERO
    !     !> Sodium pool temperature change over timestep, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTS2 = ZERO
    !     !> Sodium pool temperature change over timestep, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTS3 = ZERO
    !     !> Sodium pool temperature change over timestep, center of fourth (bottom) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTS4 = ZERO
    !     !> Wall liner temperature change over timestep, center of first (inside) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTWC1 = ZERO
    !     !> Cell wall temperature change over timestep, center of second node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTWC2 = ZERO
    !     !> Cell wall temperature change over timestep, center of third node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTWC3 = ZERO
    !     !> Cell wall temperature change over timestep, center of fourth (outside) node, \f$\us{\rankine}\f$
    !     real(kind=WP) :: DTWC4 = ZERO

    !     !> Convective heat transfer 'admittance' from sodium surface to gas, \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YCONV1 = ZERO
    !     !> Convective heat transfer 'admittance' from gas to wall liner, \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YCONV2 = ZERO

    !     !> Radiative heat transfer 'admittance' from wall liner (node 1) to node 2, \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YRAD1 = ZERO
    !     !> Radiative heat transfer 'admittance' from floor liner (node 1) to node 2, \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YRAD2 = ZERO
    !     !> Radiative heat transfer 'admittance' from sodium pool to gas, \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YRAD3 = ZERO
    !     !> Radiative heat transfer 'admittance' from sodium pool to wall liner (node 1), \f$\us{\rankine\per\BTU}\f$
    !     real(kind=WP) :: YROD = ZERO

    !     ! Summary results
    !     !> Summary value, scenario time, \f$\si{\hour}\f$
    !     real(kind=WP) :: ST = ZERO
    !     !> Summary value, cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ gauge
    !     real(kind=WP) :: SPGASC = ZERO
    !     !> Summary value, liner floor temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STF1 = ZERO
    !     !> Summary value, cell floor temperature, center of second node, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STF2 = ZERO
    !     !> Summary value, cell gas temperature, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STGASC = ZERO
    !     !> Summary value, sodium pool temperature, center of first (top) node, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STS1 = ZERO
    !     !> Summary value, liner wall temperature, center of first (inside) node, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STWC1 = ZERO
    !     !> Summary value, cell wall temperature, center of second node, \f$\us{\fahrenheit}\f$
    !     real(kind=WP) :: STWC2 = ZERO
    !     !> Summary value, sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
    !     real(kind=WP) :: SXM = ZERO

    ! contains
    !     !> Initialize results
    !     procedure :: init => result_dataframe_init
    !     !> Write initial rates in the original tabular output format
    !     procedure :: write_legacy_initial_rates => result_dataframe_write_legacy_initial_rates
    !     !> Write legacy tabular output to stdout
    !     procedure :: write_legacy_output => result_dataframe_write_legacy_output
    !     !> Write header to CSV output file
    !     procedure, nopass :: write_csv_header => result_dataframe_write_csv_header
    !     !> Write data to CSV output file
    !     procedure :: write_csv_data => result_dataframe_write_csv_data
    !     !> Initialize temperatures from initial heat sink and gas conditions
    !     procedure :: init_temperatures => result_dataframe_init_temperatures
    !     !> Update element temperature differences and temperatures
    !     procedure :: update_temperatures => result_dataframe_update_temperatures
    !     !> Update convective heat transfer
    !     procedure :: update_qconv => result_dataframe_update_qconv
    !     !> Update convective 'admittance'
    !     procedure :: update_yconv => result_dataframe_update_yconv
    !     !> Update radiative heat transfer
    !     procedure :: update_qrad => result_dataframe_update_qrad
    !     !> Update radiative 'admittance'
    !     procedure :: update_yrad => result_dataframe_update_yrad
    !     !> Calculate passive pressure-driven leakage and forced outflow and
    !     !! update the resulting cell mass, density, O2 mass and concentration,
    !     !! and beginning-of-timestep gas pressure and temperature
    !     procedure :: update_gas_flows => result_dataframe_update_gas_flows
    !     !> Update burning rate and mass of oxygen and sodium consumed
    !     procedure :: update_reactant_mass => result_dataframe_update_reactant_mass
    !     !> Update cell gas mass, density, oxygen concentration, aerosol mass,
    !     !! and pressure
    !     procedure :: update_final_gas_composition => result_dataframe_update_final_gas_composition
    !     !> Update legacy summary info fields
    !     procedure :: update_summary_info => result_dataframe_update_summary_info
    !     !> Write header to summary info CSV file
    !     procedure, nopass :: write_summary_csv_header => result_dataframe_write_summary_csv_header
    !     !> Write data to summary info CSV file
    !     procedure :: write_summary_csv_data => result_dataframe_write_summary_csv_data
    ! end type result_dataframe

    ! ----- scenario_dataframe

    ! !> Data structure containing all scenario initial conditions
    ! type, public :: scenario_dataframe
    !     !> Scenario title
    !     character(len=72) :: TITLE = ''

    !     !> Number of numerical parameters associated with scenario
    !     integer :: NPARAMS = 87

    !     !> Initial sodium surface temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TSI = ZERO
    !     !> Initial sodium temperature, node 1, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS1I = ZERO
    !     !> Initial sodium temperature, node 2, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS2I = ZERO
    !     !> Initial sodium temperature, node 3, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS3I = ZERO
    !     !> Initial sodium temperature, node 4, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TS4I = ZERO
    !     !> Initial cell floor temperature, node 1, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF1I = ZERO
    !     !> Initial cell floor temperature, node 2, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF2I = ZERO
    !     !> Initial cell floor temperature, node 3, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF3I = ZERO
    !     !> Initial cell floor temperature, node 4, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TF4I = ZERO
    !     !> Initial cell wall temperature, node 1, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC1I = ZERO
    !     !> Initial cell wall temperature, node 2, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC2I = ZERO
    !     !> Initial cell wall temperature, node 3, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC3I = ZERO
    !     !> Initial cell wall temperature, node 4, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TWC4I = ZERO
    !     !> Initial cell gas temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TGASCI = ZERO
    !     !> Initial cell gas pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
    !     real(kind=WP) :: PGASCI = ZERO
    !     !> Initial time, \f$\si{\hour}\f$
    !     real(kind=WP) :: TI = ZERO
    !     !> Ambient temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: TA = ZERO
    !     !> Ambient pressure, \f$\us{\lbf\per\square\foot}\f$ absolute
    !     real(kind=WP) :: PA = ZERO
    !     !> Initial sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
    !     real(kind=WP) :: XMI = ZERO
    !     !> Problem end time, \f$\si{\hour}\f$
    !     real(kind=WP) :: XMAX = ZERO
    !     !> Emissivity * view factor, sodium pool surface to cell walls
    !     real(kind=WP) :: AF = ZERO
    !     !> Surface area of sodium pool, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: A1 = ZERO
    !     !> Exposed cell wall surface area, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: A2 = ZERO
    !     !> Wetted area of sodium pool, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: A5 = ZERO
    !     !> Ambient oxygen concentration, volume-percent
    !     real(kind=WP) :: CO = ZERO
    !     !> Stoichiometric sodium combustion ratio \f$\us{\lbm\-\ce{Na}\per\lbm-\ce{O2}}\f$
    !     real(kind=WP) :: ANA = ZERO
    !     !> Heat of combustion, \f$\us{\BTU\per\lbm}\f$
    !     real(kind=WP) :: QC = ZERO
    !     !> Cell free volume, \f$\us{\cubic\foot}\f$
    !     real(kind=WP) :: VOLAC = ZERO
    !     !> Radiation exchange, floor liner to floor node 2, dimensionless
    !     !! \todo Should this actually be documented as "liner to wall/floor node emissivity and view factor"
    !     real(kind=WP) :: AF2 = ZERO
    !     !> Radiation exchange, sodium pool to gas, dimensionless
    !     real(kind=WP) :: AF3 = ZERO
    !     !> Mass of sodium spilled, \f$\us{\lbm}\f$
    !     real(kind=WP) :: SOD = ZERO
    !     !> Timesteps between transient output print during time interval \f$t \leq \SI{1}{\hour}\f$
    !     real(kind=WP) :: PRT1 = ZERO
    !     !> Timesteps between transient output print during time interval \f$\SI{1}{\hour} < t \leq \SI{3}{\hour}\f$
    !     real(kind=WP) :: PRT2 = ZERO
    !     !> Timesteps between transient output print during time interval \f$t > \SI{3}{\hour}\f$
    !     real(kind=WP) :: PRT3 = ZERO
    !     !> Cell exhaust volumetric flow rate, \f$\us{\cubic\foot\per\hour}\f$
    !     real(kind=WP) :: F3 = ZERO
    !     !> Pressure leakage factor, \f$\us{\cubic\foot\per\hour}\ \text{per} \sqrt{\us{\inch}\ \text{(water column)}}\f$
    !     real(kind=WP) :: CON = ZERO
    !     !> Time step stabilizer, dimensionless
    !     real(kind=WP) :: X = ZERO
    !     !> Sodium removal rate, sodium node 1, \f$\us{\lbm\per\hour}\f$
    !     real(kind=WP) :: S1 = ZERO
    !     !> Sodium removal rate, sodium node 2, \f$\us{\lbm\per\hour}\f$
    !     real(kind=WP) :: S2 = ZERO
    !     !> Sodium removal rate, sodium node 3, \f$\us{\lbm\per\hour}\f$
    !     real(kind=WP) :: S3 = ZERO
    !     !> Sodium removal rate, sodium node 4, \f$\us{\lbm\per\hour}\f$
    !     real(kind=WP) :: S4 = ZERO
    !     !> Sodium surface layer thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZNAS = ZERO
    !     !> Sodium node 1 layer thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZNAS1 = ZERO
    !     !> Sodium node 2 layer thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZNAS2 = ZERO
    !     !> Sodium node 3 layer thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZNAS3 = ZERO
    !     !> Sodium node 4 layer thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZNAS4 = ZERO
    !     !> Pan (floor node 1) thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZF1 = ZERO
    !     !> Floor node 2 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZF2 = ZERO
    !     !> Floor node 3 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZF3 = ZERO
    !     !> Floor node 4 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: ZF4 = ZERO
    !     !> Wall liner (node 1) thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: XWC1 = ZERO
    !     !> Wall node 2 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: XWC2 = ZERO
    !     !> Wall node 3 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: XWC3 = ZERO
    !     !> Wall node 4 thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: XWC4 = ZERO
    !     !> Specific heat of air, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPAC = ZERO
    !     !> Specific heat of sodium, \f$\us{\BTU\per\lbm\per\fahrenheit}\f$
    !     real(kind=WP) :: CPS = ZERO
    !     !> Specific heat of burn pan (floor node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPF1 = ZERO
    !     !> Specific heat of floor node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPF2 = ZERO
    !     !> Specific heat of floor node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPF3 = ZERO
    !     !> Specific heat of floor node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPF4 = ZERO
    !     !> Specific heat of wall liner (node 1), \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPWC1 = ZERO
    !     !> Specific heat of wall node 2, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPWC2 = ZERO
    !     !> Specific heat of wall node 3, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPWC3 = ZERO
    !     !> Specific heat of wall node 4, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: CPWC4 = ZERO
    !     !> Thermal conductivity of sodium, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKS = ZERO
    !     !> Thermal conductivity of pan (floor node 1), \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKF1 = ZERO
    !     !> Thermal conductivity of air gap between floor nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKF12 = ZERO
    !     !> Thermal conductivity of floor node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKF2 = ZERO
    !     !> Thermal conductivity of floor node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKF3 = ZERO
    !     !> Thermal conductivity of floor node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKF4 = ZERO
    !     !> Thermal conductivity of wall node 1, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKWC1 = ZERO
    !     !> Thermal conductivity of air gap between wall nodes 1 and 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKWC12 = ZERO
    !     !> Thermal conductivity of wall node 2, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKWC2 = ZERO
    !     !> Thermal conductivity of wall node 3, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKWC3 = ZERO
    !     !> Thermal conductivity of wall node 4, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: AKWC4 = ZERO
    !     !> Cell gas density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHOA = ZERO
    !     !> Sodium density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHS = ZERO
    !     !> Burn pan (floor node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHF1 = ZERO
    !     !> Floor node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHF2 = ZERO
    !     !> Floor node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHF3 = ZERO
    !     !> Floor node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHF4 = ZERO
    !     !> Wall liner (node 1) density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHWC1 = ZERO
    !     !> Wall node 2 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHWC2 = ZERO
    !     !> Wall node 3 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHWC3 = ZERO
    !     !> Wall node 4 density, \f$\us{\lbm\per\cubic\foot}\f$
    !     real(kind=WP) :: RHWC4 = ZERO
    !     !> Gap width between burn pan (floor node 1) and floor node 2, \f$\us{foot}\f$
    !     real(kind=WP) :: ZF12 = ZERO
    !     !> Gap width between cell liner (wall node 1) and wall node 2, \f$\us{foot}\f$
    !     real(kind=WP) :: XWC12 = ZERO
    ! contains
    !     !> Initialize scenario data
    !     procedure :: init => scenario_dataframe_init
    !     !> Populate scenario attributes from array
    !     procedure :: set_from_array => scenario_dataframe_set_from_array
    !     !> Populate heatsinks and heat conduction paths from scenario attributes
    !     procedure :: populate_heatnet => scenario_dataframe_populate_heatnet
    !     !> Return number of timesteps to calculate before printing output based
    !     !! on current problem time
    !     procedure :: set_legacy_print_interval => scenario_dataframe_set_legacy_print_interval
    ! end type scenario_dataframe

    ! ----- timestep_dataframe

    ! !> Structure for setting and tracking local and global timestep information
    ! type, public :: timestep_dataframe
    !     !> Limiting timestep for cell wall liner (node 1), \f$\us{\hour}\f$
    !     real(kind=WP) :: DELC1 = BIG1P9
    !     !> Limiting timestep for cell wall node 2, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELC2 = BIG1P9
    !     !> Limiting timestep for cell wall node 3, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELC3 = BIG1P9
    !     !> Limiting timestep for cell wall node 4, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELC4 = BIG1P9
    !     !> Limiting timestep for cell floor liner (node 1), \f$\us{\hour}\f$
    !     real(kind=WP) :: DELF1 = BIG1P9
    !     !> Limiting timestep for cell floor node 2, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELF2 = BIG1P9
    !     !> Limiting timestep for cell floor node 3, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELF3 = BIG1P9
    !     !> Limiting timestep for cell floor node 4, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELF4 = BIG1P9
    !     !> Limiting timestep for cell gas, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELGC = BIG1P9
    !     !> Limiting timestep for sodium surface, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELS  = BIG1P9
    !     !> Limiting timestep for sodium pool node 1, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELS1 = BIG1P9
    !     !> Limiting timestep for sodium pool node 2, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELS2 = BIG1P9
    !     !> Limiting timestep for sodium pool node 3, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELS3 = BIG1P9
    !     !> Limiting timestep for sodium pool node 4, \f$\us{\hour}\f$
    !     real(kind=WP) :: DELS4 = BIG1P9
    !     !> User-defined minimum allowable timestep, \f$\us{\hour}\f$
    !     real(kind=WP) :: dtlimit = TINY1M6
    ! contains
    !     !> Initialize timestep object attributes
    !     procedure :: init => timestep_dataframe_init
    !     !> Update minimum allowable timesteps for each element or phenomenon
    !     procedure :: update => timestep_dataframe_update
    !     !> Retrieve global minimum allowable timestep
    !     procedure :: get_dt => timestep_dataframe_get_dt
    ! end type timestep_dataframe

    ! ----- heatsink_dataframe

    ! !> One-dimensional rectilinear slab heat sink with constant properties
    ! type, public :: heatsink_dataframe
    !     !> Heat sink identifying name
    !     character(len=32) :: name = 'DEFAULT HEAT SINK              '
    !     !> Heat transfer face area, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: ahx = ZERO
    !     !> Slab thickness, \f$\us{\foot}\f$
    !     real(kind=WP) :: thickness = ZERO
    !     !> Mass, \f$\us{\lbm}\f$
    !     real(kind=WP) :: mass = ZERO
    !     !> Specific heat \f$C_{p}\f$, \f$\us{\BTU\per\lbm\per\rankine}\f$
    !     real(kind=WP) :: cp = ZERO
    !     !> Thermal conductivity \f$k\f$, \f$\us{\BTU\per\hour\per\foot\per\rankine}\f$
    !     real(kind=WP) :: k = ZERO
    !     !> Initial temperature, \f$\us{\rankine}\f$
    !     real(kind=WP) :: T0 = ZERO

    ! contains
    !     !> Initialize heatsink object attributes
    !     procedure :: init => heatsink_dataframe_init
    !     !> Add or remove mass from heatsink; added mass enters with same
    !     !! properties as heatsink (isothermal)
    !     procedure :: add_isothermal_mass => heatsink_dataframe_add_mass
    !     !> Calculate heatsink volume, \f${\us{\cubic\foot}}\f$
    !     procedure :: volume => heatsink_dataframe_volume
    !     !> Calculate heatsink density, \f${\us{\lbm\per\cubic\foot}}\f$
    !     procedure :: density => heatsink_dataframe_density
    !     !> Calculate heatsink full slab thermal conductance
    !     !! \f$\frac{k A}{x}\f$, \f${\us{\BTU\per\rankine}}\f$
    !     procedure :: th_conductance => heatsink_dataframe_thermal_conductance
    !     !> Calculate heatsink half-slab thermal resistance
    !     !! \f$\frac{x}{2 k A}\f$, \f${\us{\rankine\per\BTU}}\f$
    !     procedure :: half_resistance => heatsink_dataframe_half_resistance
    !     !> Calculate heatsink thermal capacity
    !     !! \f$m c_{p}\f$, \f$\us{\BTU\per\rankine}\f$
    !     procedure :: th_capacity => heatsink_dataframe_thermal_capacity
    ! end type heatsink_dataframe

    ! ----- qcond_dataframe

    !> Centroid-to-centroid heat conduction path between two heatsinks
    !! with optional gap conductance.
    !!
    !! Gap conductance is only calculated if any all gap characteristics
    !! are positive (non-zero). If only one heat sink is specified,
    !! conduction path is centroid-to-edge of the specified heat sink.
    !! If no heat sinks are specified, zero conductance is returned;
    !! this may be viewed as an error condition.
    ! type, public :: qcond_dataframe
    !     !> Heat sink identifying name
    !     character(len=32) :: name = 'UNINITIALIZED CONDUCTION PATH   '
    !     !> Source heatsink
    !     type(heatsink_dataframe), pointer :: hs1 => NULL()
    !     !> Destination heatsink
    !     type(heatsink_dataframe), pointer :: hs2 => NULL()
    !     !> Gap heat transfer area, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: ahxgap = ZERO
    !     !> Gap width, \f$\us{\foot}\f$
    !     real(kind=WP) :: xgap = ZERO
    !     !> Gap thermal conductivity, \f$\us{\BTU\per\foot\per\rankine}\f$
    !     real(kind=WP) :: kgap = ZERO
    ! contains
    !     !> Initialize heat conduction path attributes
    !     procedure :: init => qcond_dataframe_init
    !     !> Return gap resistance, \f$\us{\rankine\per\BTU}\f$
    !     procedure :: gap_resistance => qcond_dataframe_gap_resistance
    !     !> Return thermal conductance composed of source and destination half-resistance
    !     !! and gap resistance (if any), \f$\us{\BTU\per\rankine}\f$
    !     procedure :: conductance => qcond_dataframe_conductance
    ! end type qcond_dataframe

    ! ----- qconv_dataframe

    ! !> Heat convection path between a heatsink and a gas volume
    ! type, public :: qconv_dataframe
    !     !> Heat sink identifying name
    !     character(len=32) :: name = 'UNINITIALIZED CONVECTION PATH   '
    !     !> Correlation coefficient
    !     real(kind=WP) :: coeff
    !     !> Heat transfer surface area, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: ahx
    ! contains
    !     !> Initialize heat convection path attributes
    !     procedure :: init => qconv_dataframe_init
    !     !> Calculate convective heat transfer
    !     procedure :: qconvection => qconv_dataframe_qconvection
    ! end type qconv_dataframe

    ! ----- qrad_dataframe

    ! !> Radiation heat transfer path between two heatsinks
    ! type, public :: qrad_dataframe
    !     !> Heat transfer path identifying name
    !     character(len=32) :: name = 'UNINITIALIZED RADIATION PATH    '
    !     !> Thermal emissivity, dimensionless
    !     real(kind=WP) :: emissivity = ZERO
    !     !> Radiating area, \f$\us{\square\foot}\f$
    !     real(kind=WP) :: ahx = ZERO
    ! contains
    !     !> Initialize heat radiation path attributes
    !     procedure :: init => qrad_dataframe_init
    !     !> Radiative heat transferred from emitter to receiver
    !     procedure :: qradiation => qrad_dataframe_qrad
    ! end type qrad_dataframe

    ! Print summary at the end
    call printsummary(test)
    call jsonwritetofile(test, "c1_dataframe.json")

    call test%checkfailure()
end program c1_dataframe
