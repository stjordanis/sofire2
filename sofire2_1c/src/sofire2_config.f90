!> @file sofire2_config.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Stores application configuration information
module sofire2_config
    implicit none

    private

    public :: appconfig
    public :: sofire_cfg
    public :: FILE_STDIN

    !> Canary to indicate filename represents standard input instead
    !! of a literal filename
    character(len=*), parameter :: FILE_STDIN = '__STDIN__'

    !> Application configuration structure
    type :: appconfig
        !> Show usage flag
        logical :: show_usage = .false.
        !> Show version flag
        logical :: show_version = .false.
        !> Verbose flag
        logical :: verbose = .false.
        !> No operation 'dry run' flag
        logical :: dry_run = .false.
        !> Scenario name
        character(len=32) :: scenario = ''
        !> Input file name
        character(len=80) :: input_file = ''
        !> CEA2 thermodynamic data library file name
        character(len=80) :: thermo_file = 'thermo.inp'
        !> CEA2 transport data library file name
        character(len=80) :: transport_file = 'trans.inp'
    contains
        !> Initialize contents of appconfig object
        procedure :: init => appconfig_init
        !> Dump contents of appconfig object
        procedure :: dump => appconfig_dump
        !> Infer input file name from scenario name
        procedure :: infer_input_file => appconfig_infer_input_file
        !> Infer scenario name from application name and current date
        !! and time
        procedure :: infer_fallback_scenario =>                         &
            appconfig_infer_fallback_scenario
        !> Infer scenario name from input file name if possible
        procedure :: infer_scenario_from_input =>                       &
            appconfig_infer_scenario_from_input
        !> Indicate code should read from standard input instead of treating
        !! input_file attribute as a literal file name
        procedure :: set_input_to_stdin => appconfig_set_input_to_stdin
        !> Detect if code should read from standard input instead of treating
        !! input_file attribute as a literal file name
        procedure :: reads_from_stdin => appconfig_reads_from_stdin
    end type appconfig

    type(appconfig) :: sofire_cfg

contains

!> Initialize contents of appconfig object
subroutine appconfig_init(this)
    implicit none
    !> Object reference
    class(appconfig), intent(inout) :: this
    continue

    this%show_usage = .false.
    this%show_version = .false.
    this%verbose = .false.
    this%dry_run = .false.
    this%scenario = ''
    this%input_file = ''
    this%thermo_file = 'thermo.inp'
    this%transport_file = 'trans.inp'

    return
end subroutine appconfig_init

!> Dump contents of appconfig object
subroutine appconfig_dump(this)
    use iso_fortran_env, only: stderr => ERROR_UNIT
    use m_format, only: fmt_a
    use m_textio, only: munch, ltoa
    implicit none
    !> Object reference
    class(appconfig), intent(in) :: this
    continue

    write(unit=stderr, fmt=fmt_a) 'show_usage:     [' // ltoa(this%show_usage) // ']'
    write(unit=stderr, fmt=fmt_a) 'show_version:   [' // ltoa(this%show_version) // ']'
    write(unit=stderr, fmt=fmt_a) 'verbose:        [' // ltoa(this%verbose) // ']'
    write(unit=stderr, fmt=fmt_a) 'dry_run:        [' // ltoa(this%dry_run) // ']'
    write(unit=stderr, fmt=fmt_a) 'scenario:       [' // munch(this%scenario) // ']'
    write(unit=stderr, fmt=fmt_a) 'input_file:     [' // munch(this%input_file) // ']'
    write(unit=stderr, fmt=fmt_a) 'thermo_file:    [' // munch(this%thermo_file) // ']'
    write(unit=stderr, fmt=fmt_a) 'transport_file: [' // munch(this%transport_file) // ']'

    return
end subroutine appconfig_dump

!> Indicate code should read from standard input instead of treating
!! input_file attribute as a literal file name
subroutine appconfig_set_input_to_stdin(this)
    use m_textio, only: munch
    implicit none
    !> Object reference
    class(appconfig), intent(inout) :: this
    continue

    this%input_file = FILE_STDIN

    return
end subroutine appconfig_set_input_to_stdin

!> Detect if code should read from standard input instead of treating
!! input_file attribute as a literal file name
logical function appconfig_reads_from_stdin(this)                       &
    result(reads_from_stdin)
    use m_textio, only: munch
    implicit none
    !> Object reference
    class(appconfig), intent(in) :: this
    continue

    reads_from_stdin = (munch(this%input_file) == FILE_STDIN)

    return
end function appconfig_reads_from_stdin

!> Set input file to standard input (stdin)
subroutine appconfig_infer_input_file(this)
    use m_textio, only: munch
    implicit none
    !> Object reference
    class(appconfig), intent(inout) :: this
    continue

    this%input_file = munch(this%scenario) // '.inp'

    return
end subroutine appconfig_infer_input_file

!> Infer scenario name from application name and current date and time
subroutine appconfig_infer_fallback_scenario(this)
    use sofire2_version, only: CODENAME
    use m_textio, only: timestamp
    implicit none
    !> Object reference
    class(appconfig), intent(inout) :: this
    continue

    this%scenario = CODENAME // '_' // timestamp()

    return
end subroutine appconfig_infer_fallback_scenario

!> Infer scenario name from input file name if possible
subroutine appconfig_infer_scenario_from_input(this)
    use m_textio, only: basename, munch
    implicit none
    !> Object reference
    class(appconfig), intent(inout) :: this
    continue

    this%scenario = basename(this%input_file)
    if (munch(this%scenario) == '__BASENAME__') then
        call this%infer_fallback_scenario()
    end if

    return
end subroutine appconfig_infer_scenario_from_input


end module sofire2_config