!> @file sofire2_1c.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Main routine of SOFIRE II one-cell sodium pool fire simulator

!     SOFIRE II ENGINEERING CODE-SODIUMPOOL FIRE CLOSED CELL
!     REVISED 8-3-72

!> @brief Revised SOFIRE II one-cell sodium pool fire simulator
!! based on the 8/3/1972 source code published in
!! @cite Beiriger1973
program SOFIRE2_1C
    use iso_fortran_env, only: stdin => input_unit,                     &
        stdout => output_unit, stderr => error_unit
    use sofire2_param, only: WP, ND, n_hs_wall, n_hs_floor,             &
        n_hs_na_pool, n_qcond_path, n_qconv_path, n_qrad_path,          &
        use_legacy_rgas
    use sofire2_version, only: CODENAME, VERSION, BUILDDATE
    use sofire2_log, only: initialize_logs, finalize_logs, log_debug,   &
        log_info, log_error
    use sofire2_manifest, only: manifest_t
    use sofire2_cli, only: set_config_from_args
    use sofire2_config, only: sofire_cfg
    use m_dataframe, only: result_dataframe, scenario_dataframe,        &
        timestep_dataframe, heatsink_dataframe, qcond_dataframe,        &
        qconv_dataframe, qrad_dataframe
    use m_input, only: read_labels, read_values, get_input_unit,        &
        close_input_unit
    use m_output, only: get_csv_file, get_csv_unit, close_csv_unit
    use m_constant, only: ZERO
    use m_format, only: fmt_a
    use m_textio, only: munch, itoa
    use m_inventory, only: material_dist, parcel, get_nf_dry_air,       &
        as_mf, new_aerosol_parcel
    use m_cell, only: cell
    use m_material, only: material_db, aid_Na, gid_O2
    use m_sofire_c1, only: set_initial_conditions,                      &
        calculate_single_timestep, write_new_model_state

    implicit none

    logical :: HALT

    type(manifest_t) :: file_manifest

    type(scenario_dataframe) :: scenario

    type(result_dataframe) :: result_prev
    type(result_dataframe) :: result_curr

    type(timestep_dataframe) :: dt_manager

    ! Set heat sink dataframes as pointer targets for referencing in
    ! qcond_path
    type(heatsink_dataframe), dimension(n_hs_wall), target :: hs_wall
    type(heatsink_dataframe), dimension(n_hs_floor), target :: hs_floor
    type(heatsink_dataframe), dimension(n_hs_na_pool), target ::        &
        hs_na_pool
    type(heatsink_dataframe), target :: hs_na_crust

    type(qcond_dataframe), dimension(n_qcond_path) :: qcond_path
    type(qconv_dataframe), dimension(n_qconv_path) :: qconv_path
    type(qrad_dataframe), dimension(n_qrad_path) :: qrad_path

    real(kind=WP) :: PGASC_adjusted
    real(kind=WP) :: TGASC_adjusted

    type(material_dist) :: mf_dry_air

    ! Replacement for result_*%W
    type(cell) :: cell_prev
    type(cell) :: cell_curr

    type(parcel) :: crust_prev
    type(parcel) :: crust_curr

    ! Flow of control and diagnostics
    logical :: EXX_error
    ! Current string count is 360 + 4 EOL; padded to 400 for safety
    character(len=400) :: errmsg

    ! Input variables to work around COMMON block memory/access games in
    ! original code
    character(len=72) :: TITLE
    ! Dynamic allocation
    character(len=8), dimension(:), allocatable :: LABEL
    real(kind=WP), dimension(:), allocatable :: SAVINS
    real(kind=WP), dimension(:), allocatable :: PUTINS
    integer :: imemstatus
    ! Static allocation
    ! character(len=8), dimension(ND) :: LABEL
    ! real(kind=WP), dimension(ND) :: SAVINS
    ! real(kind=WP), dimension(ND) :: PUTINS

    ! Print interval calculation
    integer :: prtct
    integer :: prtlimit

    ! Input reading
    integer :: inunit
    integer :: iostatus

    ! Case count and CSV output support
    integer :: icase
    character(len=80) :: csvfn
    integer :: csvunit

! Formats

500 format('ERROR: Cannot allocate memory for ', A, ' - exiting.')

    continue

    HALT = .false.
    call set_config_from_args(sofire_cfg, HALT)
    if (HALT) then
        stop
    end if

    ! Initialize manifest
    call file_manifest%init()

    call initialize_logs(sofire_cfg%scenario, CODENAME, VERSION,        &
        BUILDDATE, file_manifest)

    call log_info("Populating material database")
    call material_db%init()
    material_db%transfn = sofire_cfg%transport_file
    material_db%thermofn = sofire_cfg%thermo_file
    call material_db%populate(sofire_cfg%verbose)
    ! Add material_db%transfn to manifest - ARCHIVE
    call file_manifest%add(filename=material_db%transfn,                &
        archivable=.true., generated=.false.)
    ! Add material_db%thermofn to manifest - ARCHIVE
    call file_manifest%add(filename=material_db%thermofn,               &
        archivable=.true., generated=.false.)

    mf_dry_air = as_mf(get_nf_dry_air())

    call log_info("Dynamically allocating input arrays")

    ! Dynamically allocate LABEL array
    imemstatus = 0
    if (allocated(LABEL)) then
        deallocate(LABEL)
    end if
    allocate(LABEL(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'LABEL'
        call log_error("Cannot allocate memory for LABEL")
        goto 999
    endif

    ! Dynamically allocate SAVINS array
    imemstatus = 0
    if (allocated(SAVINS)) then
        deallocate(SAVINS)
    end if
    allocate(SAVINS(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'SAVINS'
        call log_error("Cannot allocate memory for SAVINS")
        goto 999
    endif

    ! Dynamically allocate PUTINS array
    imemstatus = 0
    if (allocated(PUTINS)) then
        deallocate(PUTINS)
    end if
    allocate(PUTINS(ND), stat=imemstatus)
    if (imemstatus /= 0) then
        write(unit=stderr, fmt=500) 'PUTINS'
        call log_error("Cannot allocate memory for PUTINS")
        goto 999
    endif

    call log_info("Successful allocated memory for input arrays")
    if (sofire_cfg%reads_from_stdin()) then
        inunit = stdin
        call log_info("Reading from " // munch(sofire_cfg%input_file)   &
            // " on unit " // itoa(inunit) // " (STDIN)" )
    else
        inunit = stdin
        iostatus = 0
        errmsg = 'OK'
        call get_input_unit(munch(sofire_cfg%input_file), inunit,       &
            iostatus, errmsg)
        if (iostatus == 0) then
            call log_info("Reading from "                               &
                // munch(sofire_cfg%input_file) // " on unit "          &
                // itoa(inunit) )
            ! Add sofire_cfg%input_file to manifest - ARCHIVE
            call file_manifest%add(filename=sofire_cfg%input_file,      &
                archivable=.true., generated=.false.)
        else
            call log_error("Cannot read from "                          &
                // munch(sofire_cfg%input_file) // " - "                &
                // itoa(iostatus) // ": " // munch(errmsg))
            goto 999
        end if
    end if

    ! Read scenario initial condition labels
    SAVINS = ZERO
    LABEL = '        '
    call read_labels(inunit, ND, LABEL)

    call scenario%init()

    call result_prev%init()
    call result_curr%init()

    ! Gas cell
    call cell_prev%init()
    call cell_curr%init()

    ! Sodium pool surface
    crust_prev = new_aerosol_parcel()
    crust_curr = new_aerosol_parcel()

    call dt_manager%init()

    icase = 0

    call log_debug("Entering scenario loop L1")

    ! Calculate the next scenario, if any
L1: do
        ! Read initial condition title and values of next scenario.
        ! If this fails on read error, etc., all input conditions will
        ! be set to zero including scenario end time (XMAX).
        ! Gracefully exit if XMAX is not greater than zero.
        PUTINS = ZERO
        call read_values(inunit, ND, LABEL, PUTINS, SAVINS, TITLE)
        call scenario%set_from_array(PUTINS)
        scenario%TITLE = TITLE

        ! Exit gracefully if scenario end time is not positive
        if (scenario%XMAX <= ZERO) then
            call log_info("Scenario end time is less than zero - "      &
                // "exiting")
            exit L1
        end if

        ! Set initial conditions

        call log_debug("Setting initial conditions for scenario ["      &
            // munch(scenario%TITLE) // "]")

        ! Set up beginning-of-timestep values for first timestep
        call set_initial_conditions(scenario, result_curr, cell_curr,   &
            crust_curr, dt_manager, hs_wall, hs_floor, hs_na_pool,      &
            hs_na_crust, qcond_path, qconv_path, qrad_path, mf_dry_air, &
            use_legacy_rgas)

        ! Set CSV file name and output unit number from case ID and
        ! scenario name
        csvfn = get_csv_file(icase, sofire_cfg%scenario)
        csvunit = get_csv_unit(csvfn)
        ! Add csvfn to manifest - ARCHIVE, GENERATED
        call file_manifest%add(filename=csvfn, archivable=.true.,       &
            generated=.true.)
        call log_info("Writing CSV output to " // munch(csvfn)          &
            // " on unit " // itoa(csvunit))

        ! Display initial rates
        call result_curr%write_legacy_initial_rates(stdout, dt_manager)

        call result_curr%update_summary_info(scenario%PA)

        ! Display T=0 results (initial quiescient conditions)
        call result_curr%write_legacy_output(stdout)

        if (sofire_cfg%verbose) then
            call write_new_model_state(cell_curr, crust_curr)
        end if

        call result_curr%write_csv_header(csvunit)
        call result_curr%write_csv_data(csvunit)

        ! Set expected number of timesteps to iterate over BETWEEN
        ! PRODUCING OUTPUT
        prtlimit = scenario%set_legacy_print_interval(result_curr%T)
        prtct = 2

        call log_debug("Beginning scenario timestep loop L75")
L75:    do while (result_curr%T < scenario%XMAX)

            crust_prev = crust_curr
            cell_prev = cell_curr

            result_prev = result_curr

            call calculate_single_timestep(scenario, result_curr,       &
                result_prev, cell_curr, cell_prev, crust_curr,          &
                dt_manager, hs_wall, hs_floor, hs_na_pool, hs_na_crust, &
                qcond_path, qconv_path, qrad_path, PGASC_adjusted,      &
                TGASC_adjusted, mf_dry_air, prtct, errmsg, EXX_error)

                if (EXX_error) then
                    write(unit=stdout, fmt=fmt_a) munch(errmsg)
                    call close_csv_unit(csvunit)
                    call log_error("EXX_error: " // munch(errmsg))
                    cycle L1
                end if

                prtct = prtct + 1

                if (prtct > prtlimit) then
                    call result_curr%update_summary_info(scenario%PA)

                    call result_curr%write_legacy_output(stdout)

                    if (sofire_cfg%verbose) then
                        block
                            real(kind=WP) :: OXLB_cell_model
                            continue
                            OXLB_cell_model = min(                      &
                                result_curr%OXLB,                       &
                                cell_curr%gas%species(gid_O2)%mass,     &
                                crust_curr%species(aid_NA)%mass         &
                                / scenario%ANA)

                            write(6, "('OXLB: ', ES16.8, "              &
                                // "', OXLB(c): ', ES16.8, "            &
                                // "', dMNA: ', ES16.8, "               &
                                // "', dMNA(c): ', ES16.8)")            &
                                result_curr%OXLB, OXLB_cell_model,      &
                                result_curr%OXLB * scenario%ana,        &
                                crust_prev%species(aid_NA)%mass         &
                                - crust_curr%species(aid_NA)%mass
                        end block

                        call write_new_model_state(cell_curr,           &
                            crust_curr)
                    end if

                call result_curr%write_csv_data(csvunit)

                prtlimit =                                              &
                    scenario%set_legacy_print_interval(result_curr%T)
                prtct = 2
            end if

        end do L75

        ! Final timestep

        call result_curr%update_summary_info(scenario%PA)

        call result_curr%write_legacy_output(stdout)

        if (sofire_cfg%verbose) then
            call write_new_model_state(cell_curr, crust_curr)
        end if

        call result_curr%write_csv_data(csvunit)

        call close_csv_unit(csvunit)

        call log_info("Completing scenario")

        icase = icase + 1

    end do L1

999 continue

    ! Clean up and exit
    call log_info("Cleaning up before code exit")

    if (allocated(LABEL)) then
        deallocate(LABEL)
    end if
    if (allocated(SAVINS)) then
        deallocate(SAVINS)
    end if
    if (allocated(PUTINS)) then
        deallocate(PUTINS)
    end if

    ! Nullifies heatsink pointer references in heat conduction paths
    block
        integer :: i
        continue

        do i = 1, n_qcond_path
            call qcond_path(i)%init()
        end do
    end block

    ! Safe; does nothing if inunit is stdin or is already closed
    call close_input_unit(inunit)

    ! Safe; does nothing if csvunit is stdout or is already closed
    call close_csv_unit(csvunit)

    ! Write manifest
    call file_manifest%write_to_file(sofire_cfg%scenario)
    call file_manifest%init()

    call finalize_logs()

    stop
end program SOFIRE2_1C