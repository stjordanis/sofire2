!> @file m_input.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Routines for reading scenario input
module m_input

implicit none

private

public :: read_labels
public :: read_values
public :: get_input_unit
public :: close_input_unit

contains

!> @brief Input parser; reads text elements into LABEL
!!
!! Based on initial (main) code in subroutine READIN()
subroutine read_labels(UNIT, ND, LABEL)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    implicit none

! Parameters

    character(len=8), parameter :: BLANK8 = '        '
    integer, parameter :: NLABL = 5

! Arguments

    !> Input unit number
    integer, intent(in) :: UNIT

    !> Size of LABEL array
    integer, intent(in) :: ND

    !> Text labels for each element
    character(len=8), dimension(:), intent(out) :: LABEL

! Local variables

    character(len=8), dimension(NLABL) :: LABL
    integer :: I
    integer :: K
    integer :: N
    integer :: NA
    integer :: LCT
    integer :: ERRCT

    integer :: iostatus
    character(len=80) :: msg

! Formats

111 format(I1, I5, 6X, 5(A8,4X))
500 format('WARNING: Line ', I3, ', field ', I2, ': Label index (', I3, &
           ') exceeds array size (', I3, ') - skipping rest of line' )
501 format('Review input especially columns 2-6 of label section.', /,  &
           'Read is proceeding to next line of input but inconsistent', &
           ' results may be produced')
502 format('WARNING: ', I3, ' errors detected.')
503 format('ERROR: Read failure on line ', I3, ' of input. IOSTAT=',    &
            I3, ', ', A)

    continue

    LABEL(1:ND) = BLANK8

    iostatus = 0
    msg = ''
    ERRCT = 0
    LCT = 0
    N = 0
LINE: do while (N == 0)
        LCT = LCT + 1
        read(unit=UNIT, fmt=111, iostat=iostatus, iomsg=msg)            &
            N, NA, LABL(1:NLABL)
        if (iostatus /= 0) then
            ERRCT = ERRCT + 1
            write(unit=stderr, fmt=503) LCT, iostatus, munch(msg)
            exit LINE
        end if
        I = NA
        do K = 1, NLABL
            if (LABL(K) /= BLANK8) then
                if (I <= ND) then
                    LABEL(I) = LABL(K)
                    I = I + 1
                else
                    ERRCT = ERRCT + 1
                    write(unit=stderr, fmt=500) LCT, K, I, ND
                    if (ERRCT == 1) then
                        write(unit=stderr, fmt=501)
                    end if
                    cycle LINE
                endif
            end if
        end do
    end do LINE

    if (ERRCT > 1) then
        write(unit=stderr, fmt=502) ERRCT
    end if

    return
end subroutine read_labels

!> @brief Relocatable floating point data input routine.
!!
!! LOC1, index of first piece of data on card, goes in columns 7-12.
!! EDATA(1) goes in cols. 13-24, EDATA(2) in cols. 25-36, etc.
!! EDATA left blank is not changed, but must write 0.0 for zero, not .0
!! Must write a non-zero N in col. 1 of the last data card.
!!
!! @todo Improve numeric parsing and eliminate 'T13' implicit rewind
subroutine fored(UNIT, ND, EDATA, N)
    use iso_fortran_env, only: stdout => output_unit,                   &
        stderr => error_unit
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    use m_textio, only: munch
    implicit none

! Constants

    integer, parameter :: LFIELD = 12
    character(len=LFIELD), parameter :: BLANK = '            '
    integer, parameter :: NFIELDS = 5

! Arguments

    !> File input unit
    integer, intent(in) :: UNIT

    !> Size of EDATA array
    integer, intent(in) :: ND

    !> Numerical data read from file
    real(kind=WP), dimension(:), intent(inout) :: EDATA

    !> Termination code; non-zero implies normal termination of file
    !! reading. 1 or 9+ indicates a single or 'modifier' scenario (does
    !! not overwrite SAVINS), 2-8 indicates a base case (overwrites
    !! SAVINS). N < 0 indicates a fatal error.
    integer, intent(out) :: N

! Local variables

    ! Temporary storage of floating-point input data
    real(kind=WP), dimension(NFIELDS) :: ER

    ! Temporary storage of text representation of floating-point input data
    character(len=LFIELD), dimension(NFIELDS) :: CARD
    integer :: I
    integer :: L
    integer :: LOC
    integer :: LOC1

    integer :: iostatus
    character(len=80) :: msg

! Formats
11  format(I1, I5, I6, 5E12.8, T13, 5A12)
21  format('1NEGATIVE OR ZERO LOCATION ON FORED DATA CARD.')
31  format('1ERROR: Attempted to write input past end of data array: ', &
           'L:', I4, ' > ND:', I4)
500 format('1ERROR: Data read failure - I/O ERROR ', I4, ' : ', A)

    continue

    N = 0
L1: do while (N == 0)
        ER = ZERO
        CARD = BLANK
        read(unit=UNIT, fmt=11, iostat=iostatus, iomsg=msg)             &
            N, LOC, LOC1, ER(1:NFIELDS), CARD(1:NFIELDS)

        if (iostatus /= 0) then
            ! Abnormal termination
            write(unit=stdout, fmt=500) iostatus, munch(msg)
            N = -1
            exit L1
        end if

        LOC = LOC + LOC1
        if (LOC <= 0) then
            write(unit=stdout, fmt=21)
            N = -2
            exit L1
        end if

        do I = 1, NFIELDS
            if (CARD(I) == BLANK) then
                if (ER(I) == ZERO) then
                    cycle
                end if
            end if
            L = LOC + I - 1
            if (L > ND) then
                write(unit=stdout, fmt=31) L, ND
                N = -3
                exit L1
            end if
            EDATA(L) = ER(I)
        end do
    end do L1

    return
end subroutine fored

!> Reads TITLE and non-blank numerical input
!!
!! Based on entry point READ() in READIN()
subroutine read_values(UNIT, ND, LABEL, PUTINS, SAVINS, TITLE)
    use iso_fortran_env, only: stdout => output_unit,                   &
        stderr => error_unit, iostat_end
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    use m_textio, only: munch
    implicit none

! Parameters

    integer, parameter :: NLABL = 5

! Arguments

    !> Input unit number
    integer, intent(in) :: UNIT

    !> Total elements to read
    integer, intent(in) :: ND

    !> Text labels for each element
    character(len=8), dimension(:), intent(in) :: LABEL

    !> TBD value
    real(kind=WP), dimension(:), intent(out) :: PUTINS

    !> TBD value
    real(kind=WP), dimension(:), intent(inout) :: SAVINS

    !> Title of analysis
    character(len=72), intent(out) :: TITLE

! Local variables

    integer :: LA
    integer :: LB
    integer :: N
    integer :: NA

    integer :: iostatus
    character(len=80) :: msg

! Formats

201 format(A72)
202 format('1', 10X, A72)
231 format('0 DATA FOR THIS CASE ONLY. REFERENCE DATA UNCHANGED')
221 format('0 THESE INPUTS HAVE BEEN STORED AS A REFERENCE SET')
241 format('0', 7X, 5(9X, A8))
242 format(I6, 4X, 5ES17.5)
500 format('0 Note: Cannot read title from input. I/O ERROR ', I4,      &
  ' : ', A)
501 format('0 ERROR: Unrecoverable error in FORED (', I3, ')- halting.')

    continue

    iostatus = 0
    msg = 'OK'
    N = 9
    do while (N == 9)
        read(unit=UNIT, fmt=201, iostat=iostatus, iomsg=msg) TITLE
        if (iostatus /= 0) then
            if (iostatus /= iostat_end) then
                ! Abnormal termination
                write(unit=stderr, fmt=500) iostatus, munch(msg)
            end if
            ! Clear all input to force end time (XMAX) to zero
            SAVINS(1:ND) = ZERO
            PUTINS(1:ND) = ZERO
            return
        end if
        write(unit=stdout, fmt=202) TITLE
        PUTINS(1:ND) = SAVINS(1:ND)
        call fored(UNIT, ND, PUTINS, N)
        if (N < 0) then
            write(unit=stderr, fmt=501) N
            SAVINS(1:ND) = ZERO
            PUTINS(1:ND) = ZERO
            return
        elseif (N > 1 .AND. N < 9) then
            write(unit=stdout, fmt=231)
            SAVINS(1:ND) = PUTINS(1:ND)
        else
            write(unit=stdout, fmt=221)
        end if

        ! Write scenario input with labels to tabular output
        do NA = 1, ND, NLABL
            LA = NA
            LB = NA + NLABL - 1
            write(unit=stdout, fmt=241) LABEL(LA:LB)
            write(unit=stdout, fmt=242) LA, PUTINS(LA:LB)
        end do
    end do

    return
end subroutine read_values

!> Open input file for reading; set unit number
subroutine get_input_unit(filename, iunit, iostatus, msg)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    implicit none

! Arguments

    !> CSV file name
    character(len=*), intent(in) :: filename

    !> Input file unit
    integer, intent(out) :: iunit

    !> I/O status code
    integer, intent(out) :: iostatus

    !> I/O status message
    character(len=*), intent(out) :: msg

! Formats

1   format('ERROR: Tried reading from ', A, ' but got ', I0, ': ', A)

    continue

    iunit = 0
    iostatus = 0
    msg = "OK"
    open(newunit=iunit, file=filename, action='READ',                  &
        status='OLD', form='FORMATTED', iostat=iostatus, iomsg=msg)
    if (iostatus /= 0) then
        write(unit=stderr, fmt=1) munch(filename), iostatus, munch(msg)
        iunit = 0
    end if

    return
end subroutine get_input_unit

!> Close input file if it is open and it is not stdin
subroutine close_input_unit(iunit)
    use iso_fortran_env, only: stdin => INPUT_UNIT
    implicit none

! Arguments

    !> Input file unit
    integer, intent(inout) :: iunit

    logical :: is_open

    continue

    if (iunit /= stdin) then
        inquire(unit=iunit, opened=is_open)
        if (is_open) then
            close(iunit)
        end if
        iunit = stdin
    end if

    return
end subroutine close_input_unit

end module m_input
