!> @file sofire2_cli.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Provides command-line argument processing for SOFIRE2
!!
!! Underlying argument processing is implemented by command_args from
!1 the FLIBS distribution; see http://flibs.sourceforge.net/
module sofire2_cli
    implicit none

    private

    public :: display_version
    public :: display_usage
    public :: set_config_from_args

contains

!> @brief Display help / version instructions
subroutine display_version()
    use iso_fortran_env, only: stdout => OUTPUT_UNIT
    use sofire2_version, only: CODENAME, VERSION, BUILDDATE
    implicit none
1   format(A, ', ', A, ', ', A)
    continue
    write(unit=stdout, fmt=1) CODENAME, VERSION, BUILDDATE
    return
end subroutine display_version

!> @brief Display help / usage instructions
subroutine display_usage()
    use iso_fortran_env, only: stdout => OUTPUT_UNIT
    use m_format, only: fmt_a
    use m_textio, only: timestamp, munch
    use sofire2_version, only: CODENAME
    implicit none
    character(len=80) :: cmd
    continue

    call get_command_argument(number=0, value=cmd)

    write(unit=stdout, fmt=fmt_a) 'Usage:'
    write(unit=stdout, fmt=fmt_a) munch(cmd) // ' --help --usage --version --verbose --dry-run --scenario TAG'
    write(unit=stdout, fmt=fmt_a) '       --infile INPUT_FILE --thermo-file THERMO_LIB --transport-file TRANS_LIB'
    write(unit=stdout, fmt=fmt_a) 'Options:'
    write(unit=stdout, fmt=fmt_a) '  -h, -?, --help           Displays basic help and exits'
    write(unit=stdout, fmt=fmt_a) '  -u, --usage              Displays this usage message and exits'
    write(unit=stdout, fmt=fmt_a) '  -v, --version            Displays the code version and exits'
    write(unit=stdout, fmt=fmt_a) '  -V, --verbose            Enables verbose diagnostic output'
    write(unit=stdout, fmt=fmt_a) '  -n, --dry-run            Processes command line arguments and exits'
    write(unit=stdout, fmt=fmt_a) '  -s, --scenario CASENAME  Sets text label to CASENAME for deriving input and'
    write(unit=stdout, fmt=fmt_a) '                           output file names. If not set, CASENAME is inferred'
    write(unit=stdout, fmt=fmt_a) '                           from the base name of INPUT_FILE (path and extension'
    write(unit=stdout, fmt=fmt_a) '                           are removed)'
    write(unit=stdout, fmt=fmt_a) '  -i, --infile INPUT_FILE  Specifies input file name and path as INPUT_FILE.'
    write(unit=stdout, fmt=fmt_a) '                           If not set, input file is "CASENAME.inp"'
    write(unit=stdout, fmt=fmt_a) '  -H, --thermo-file THERMO_LIB    Specifies CEA2-formatted thermodynamic'
    write(unit=stdout, fmt=fmt_a) '                                  property file name and path. If not set,'
    write(unit=stdout, fmt=fmt_a) '                                  THERMO_LIB defaults to "thermo.inp"'
    write(unit=stdout, fmt=fmt_a) '  -R, --transport-file TRANS_LIB  Specifies CEA2-formatted transport property'
    write(unit=stdout, fmt=fmt_a) '                                  file name and path. If not set, TRANS_LIB'
    write(unit=stdout, fmt=fmt_a) '                                  defaults to "trans.inp"'
    write(unit=stdout, fmt=fmt_a) 'Notes:'
    write(unit=stdout, fmt=fmt_a) ' * If neither --scenario or --infile are specified, input will be read from'
    write(unit=stdout, fmt=fmt_a) '   STDIN, output will be written to STDOUT (legacy behavior). CASENAME will'
    write(unit=stdout, fmt=fmt_a) '   be set to "sofire2" for naming log, CSV, and other files.'
    write(unit=stdout, fmt=fmt_a) ' * If --infile is set to "-" or "__STDIN__", input will be read from STDIN,'
    write(unit=stdout, fmt=fmt_a) '   regardless of the setting of --scenario. In this case, if --scenario is'
    write(unit=stdout, fmt=fmt_a) '   not set, CASENAME will be set to (for exammple) "' &
        // CODENAME // '_' // timestamp() // '"'
    write(unit=stdout, fmt=fmt_a) 'Version:'
    call display_version()

    return
end subroutine display_usage

!> @brief Set application configuration from command line arguments
subroutine set_config_from_args(cfg, halt)
    use iso_fortran_env, only: stdout => OUTPUT_UNIT,                   &
        stderr => ERROR_UNIT
    use sofire2_config, only: appconfig, FILE_STDIN
    use sofire2_version, only: CODENAME
    use command_args, only: handle_command_options, optarg, opt_true,   &
        opt_value_next
    use m_format, only: fmt_a
    use m_textio, only: munch, timestamp

    implicit none

    !> Application configuration object
    class(appconfig), intent(inout) :: cfg

    !> Flag indicating code should halt
    logical, intent(out) :: halt

    character(len=:), allocatable :: m_input_file
    character(len=:), allocatable :: m_scenario

    continue

    ! -h, --help
    ! -v, --version
    ! -V, --verbose
    ! -s, --scenario
    ! -i, --infile=s (infer from -s)
    ! -H, --thermo-file=s
    ! -R, --transport-file=s

    call handle_command_options([                                       &
        optarg(cfg%scenario,       opt_value_next,                      &
        's', 'scenario', 'Secnario name'),                              &
        optarg(cfg%input_file,     opt_value_next,                      &
        'i', 'infile', 'Input file'),                                   &
        optarg(cfg%thermo_file,    opt_value_next,                      &
        'H', 'thermo-file', 'Thermodynamic property file (trans.inp)'), &
        optarg(cfg%transport_file, opt_value_next,                      &
        'R', 'transport-file', 'Transport property file (trans.inp)'),  &
        optarg(cfg%dry_run,        opt_true,                            &
        'n', 'dry-run',    'Process command-line arguments and quit'),  &
        optarg(cfg%show_usage,     opt_true,                            &
        'u', 'usage',    'Show detailed usage info and quit'),          &
        optarg(cfg%show_version,   opt_true,                            &
        'v', 'version', 'Show version and quit'),                       &
        optarg(cfg%verbose,        opt_true,                            &
        'V', 'verbose', 'Set verbose output')                           &
    ])

    if (cfg%verbose) then
        write(unit=stdout, fmt=fmt_a)                                   &
            'Initial application configuration and command-line options'
        call cfg%dump()
    end if

    if (cfg%show_usage) then
        call display_usage()
        halt = .true.
        return
    end if

    if (cfg%show_version) then
        call display_version()
        halt = .true.
        return
    end if

    m_input_file = munch(cfg%input_file)
    m_scenario = munch(cfg%scenario)

    ! UNIX convention; treat '-' as STDIN
    if (m_input_file == '-') then
        call cfg%set_input_to_stdin()
        m_input_file = munch(cfg%input_file)
    end if

    select case (m_input_file)
    case(FILE_STDIN)
        ! Input file is STDIN
        if (m_scenario == '') then
            ! No scenario; get fallback name
            call cfg%infer_fallback_scenario()
        end if
    case('')
        ! Input file is not set
        if (m_scenario == '') then
            ! No scenario; get fallback name and set input to STDIN
            ! call cfg%infer_fallback_scenario()
            ! NOTE: THIS IS COMPATIBLE WITH ORIGINAL CSV NAMING; switch
            ! to `call cfg%infer_fallback_scenario()` in next major
            ! version update
            cfg%scenario = 'sofire2'
            call cfg%set_input_to_stdin()
        else
            ! Scenario exists; infer input file name
            call cfg%infer_input_file()
        end if
    case default
        ! Input file is set and not stdin
        if (m_scenario == '') then
            ! No scenario; get fallback name
            call cfg%infer_scenario_from_input()
        end if
    end select

    if (cfg%verbose) then
        write(unit=stdout, fmt=fmt_a)                                   &
            'Post-processed application configuration:'
        call cfg%dump()
    end if

    if (cfg%dry_run) then
        halt = .true.
        write(unit=stdout, fmt=fmt_a)                                   &
            'Post-processed application configuration (dry run):'
        call cfg%dump()
    end if

    return
end subroutine set_config_from_args

end module sofire2_cli