!> @file m_cell.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Gas space control volume model
module m_cell
    use sofire2_param, only: WP
    use m_constant, only: ZERO
    use m_format, only: fmt_a
    use m_inventory, only: inventory, new_gas_inventory,                &
        new_aerosol_inventory
!    use m_material, only: material_db, gid_Na, gid_O2
    implicit none
    private

    !> Gas space control volume
    type, public :: cell
        !> Identifying name
        character(len=32) :: name = 'UNINITIALIZED CELL             '
        !> Pressure, \f$\us{\lbf\per\square\foot}\f$
        real(kind=WP) :: p = ZERO
        !> Volume, \f$\us{\cubic\foot}\f$
        real(kind=WP) :: v = ZERO
        !> Temperature, \f$\us{\rankine}\f$
        real(kind=WP) :: t = ZERO
        !> Gas inventory
        type(inventory) :: gas
        !> Aerosol inventory
        type(inventory) :: aerosol
    contains
        !> Initialize cell attributes
        procedure :: init => cell_init
        !> Set gas species masses based on ideal gas law and mass
        !! distribution `mf`
        procedure :: set_ideal_gas_mass => cell_set_ideal_gas_mass
        !> Return combined mass of gas and aerosol in cell,
        !! \f$\us{\lbm}\f$
        procedure :: mass => cell_mass
        !> Return aggregate gas-space density (both gas and aerosol
        !! mass), \f$\us{\lbm\per\cubic\foot}\f$
        procedure :: mixture_density => cell_mixture_density
        !> Return density of cell gas (does not include aerosol mass),
        !! \f$\us{\lbm\per\cubic\foot}\f$
        procedure :: gas_density => cell_gas_density
        !> Return density of cell aerosol (does not include gas mass),
        !! \f$\us{\lbm\per\cubic\foot}\f$
        procedure :: aerosol_density => cell_aerosol_density
        !> Mass fraction of oxygen in the cell
        procedure :: mf_o2 => cell_mf_o2
    end type cell

contains

!> Initialize cell dataframe
subroutine cell_init(this)
    ! use iso_fortran_env, only: stderr => error_unit
    implicit none

    !> Object reference
    class(cell), intent(inout) :: this

    continue

    this%name = 'UNINITIALIZED CELL              '

    ! call this%gas%init()
    ! call this%aerosol%init()

    this%gas = new_gas_inventory()
    ! call this%gas%mf%dump()
    ! call this%gas%nf%dump()

    this%aerosol = new_aerosol_inventory()
    ! call this%aerosol%mf%dump()
    ! call this%aerosol%nf%dump()

    this%p = ZERO
    this%v = ZERO
    this%t = ZERO

    return
end subroutine cell_init

!> Set gas mass inventory from pressure, volume, temperature, and mass
!! distribution. Requires this%gas%nf to be current prior to call
subroutine cell_set_ideal_gas_mass(this)
    ! use iso_fortran_env, only: stderr => error_unit
    use m_constant, only: RGAS_US, FTLBF_BTU
    use m_material, only: material_db
    implicit none

    !> Object reference
    class(cell), intent(inout) :: this

    integer :: i
    integer :: id
    real(kind=WP) :: mw_frac
    real(kind=WP) :: n_total
    real(kind=WP) :: m_total
    real(kind=WP) :: mw_total

    continue

    n_total = this%p * this%v / (this%t * RGAS_US * FTLBF_BTU)

    ! if (allocated(this%gas%species)) then
    !     write(unit=stderr, fmt='(A, I0, A)') 'cell%gas%species is allocated with ', size(this%gas%species), 'elements'
    ! else
    !     write(unit=stderr, fmt=fmt_a) 'cell%gas%species is NOT allocated'
    ! end if

    mw_total = ZERO
    do i = 1, size(this%gas%nf%dist)
        id = this%gas%nf%dist(i)%id
        mw_frac = this%gas%nf%dist(i)%f * material_db%gas(id)%mw
        mw_total = mw_total + mw_frac
        this%gas%species(id)%mass = n_total * mw_frac
        m_total = m_total + this%gas%species(id)%mass
    end do

    ! write(6, '("Initial mass: ", ES12.4)') m_total
    ! write(6, '("Initial O2 mass: ", ES12.4)') this%gas%species(gid_O2)%m
    ! write(6, '("Initial moles: ", ES12.4)') n_total
    ! write(6, '("Initial MWbar: ", ES12.4)') mw_total
    ! write(6, '("Initial rho: ", ES12.4)') m_total / this%v

    return
end subroutine cell_set_ideal_gas_mass

!> Return total mass of inventory
pure real(kind=WP) function cell_mass(this) result(m)
    implicit none

    !> Object reference
    class(cell), intent(in) :: this

    continue

    m = this%gas%mass() + this%aerosol%mass()

    return
end function cell_mass

!> Return aggregate gas-space density (both gas and aerosol mass),
!! \f$\us{\lbm\per\cubic\foot}\f$
pure real(kind=WP) function cell_mixture_density(this) result(rhoc)
    implicit none
    !> Object reference
    class(cell), intent(in) :: this
    continue
    if (this%v > ZERO) then
        rhoc = this%mass() / this%v
    else
        rhoc = ZERO
    end if
    return
end function cell_mixture_density

!> Return density of cell gas (does not include aerosol mass),
!! \f$\us{\lbm\per\cubic\foot}\f$
pure real(kind=WP) function cell_gas_density(this) result(rhog)
    implicit none
    !> Object reference
    class(cell), intent(in) :: this
    continue
    if (this%v > ZERO) then
        rhog = this%gas%mass() / this%v
    else
        rhog = ZERO
    end if
    return
end function cell_gas_density

!> Return density of cell aerosol (does not include gas mass),
!! \f$\us{\lbm\per\cubic\foot}\f$
pure real(kind=WP) function cell_aerosol_density(this) result(rhoa)
    implicit none
    !> Object reference
    class(cell), intent(in) :: this
    continue
    if (this%v > ZERO) then
        rhoa = this%aerosol%mass() / this%v
    else
        rhoa = ZERO
    end if
    return
end function cell_aerosol_density

!> Mass fraction of oxygen in the cell
real(kind=WP) function cell_mf_o2(this) result(mf_o2)
    use m_material, only: gid_O2
    implicit none
    !> Object reference
    class(cell), intent(in) :: this
    continue
    mf_o2 = this%gas%mf%dist(gid_O2)%f
    return
end function cell_mf_o2

end module m_cell