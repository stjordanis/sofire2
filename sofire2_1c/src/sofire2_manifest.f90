!> @file sofire2_manifest.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!!
!! @brief Track acrhivable and generated file artifacts from a SOFIRE2 run
module sofire2_manifest
    implicit none
    private

    public :: manifest_t
    public :: new_manifest_node

    !> Singly-linked list node, file manifest entry
    type :: manifest_node_t
        !> Filename
        character(len=:), allocatable :: file
        !> Flag indicating file is case input or results which should be
        !! archived for reproducability
        logical :: is_archivable = .true.
        !> Flag indicating file generated and may safely be deleted
        logical :: is_generated = .false.
        !> Pointer to next node in list
        type(manifest_node_t), pointer :: next => null()
    contains
        !> Initialize file manifest node
        procedure :: init => manifest_node_t_init
        !> Dump file manifest node internals
        procedure :: dump => manifest_node_t_dump
    end type manifest_node_t

    !> Singly-linked list, file manifest
    type :: manifest_t
        !> Head of linked list
        type(manifest_node_t), pointer :: head => null()
        !> Tail of linked list
        type(manifest_node_t), pointer :: tail => null()
    contains
        !> Initialize file manifest
        procedure :: init => manifest_t_init
        !> Dump file manifest internals
        procedure :: dump => manifest_t_dump
        !> Write file manifest contents to file unit(s)
        procedure :: write_to_unit => manifest_t_write_to_unit
        !> Write file manifest contents to file(s)
        procedure :: write_to_file =>manifest_t_write_to_file
        !> Add file and status flags to manifest
        procedure :: add => manifest_t_add
    end type manifest_t

contains

!> Initialize file manifest node.
subroutine manifest_node_t_init(this, filename, archivable, generated)
    use m_textio, only: munch
    implicit none

    !> Object reference
    class(manifest_node_t), intent(inout) :: this

    !> Filename
    character(len=*), intent(in), optional :: filename

    !> Archivable status
    logical, intent(in), optional :: archivable

    !> Generated status
    logical, intent(in), optional :: generated

    continue

    if (present(filename)) then
        this%file = munch(filename)
    else
        this%file = ''
    end if

    if (present(archivable)) then
        this%is_archivable = archivable
    else
        this%is_archivable = .true.
    end if

    if (present(generated)) then
        this%is_generated = generated
    else
        this%is_generated = .false.
    end if

    nullify(this%next)

    return
end subroutine manifest_node_t_init

!> Initialize file manifest node.
type(manifest_node_t) function new_manifest_node(filename, archivable,  &
    generated) result(mnode)
    use m_textio, only: munch
    implicit none

    !> Filename
    character(len=*), intent(in) :: filename

    !> Archivable status. Optional, defaults to .true.
    logical, intent(in), optional :: archivable

    !> Generated status. Optional, defaults to .false.
    logical, intent(in), optional :: generated

    continue

    mnode%file = munch(filename)

    if (present(archivable)) then
        mnode%is_archivable = archivable
    else
        mnode%is_archivable = .true.
    end if

    if (present(generated)) then
        mnode%is_generated = generated
    else
        mnode%is_generated = .false.
    end if

    return
end function new_manifest_node

!> Dump file manifest node internals
subroutine manifest_node_t_dump(this, i)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch
    implicit none

    !> Object reference
    class(manifest_node_t), intent(in) :: this

    !> Counter, optional
    integer, intent(in), optional :: i

    character(len=1) :: a_state
    character(len=1) :: g_state

1   format(I4, 1X, A1, 1X, A1, 1X, A)
2   format(A1, 1X, A1, 1X, A)

    continue

    if (this%is_archivable) then
        a_state = 'A'
    else
        a_state = ' '
    end if

    if (this%is_generated) then
        g_state = 'G'
    else
        g_state = ' '
    end if

    if (present(i)) then
        write(unit=stderr, fmt=1) i, a_state, g_state, this%file
    else
        write(unit=stderr, fmt=2) a_state, g_state, this%file
    end if

    return
end subroutine manifest_node_t_dump

!> Initialize file manifest
subroutine manifest_t_init(this)
    implicit none

    !> Object reference
    class(manifest_t), intent(inout) :: this

    continue

    nullify(this%head)
    nullify(this%tail)

    return
end subroutine manifest_t_init

!> Dump file manifest internals
subroutine manifest_t_dump(this)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: itoa
    use m_format, only: fmt_a
    implicit none

    !> Object reference
    class(manifest_t), intent(inout) :: this

    integer :: ict
    integer :: act
    integer :: gct

    type(manifest_node_t), pointer :: mnode

    continue

    ict = 0
    act = 0
    gct = 0

    mnode => this%head
    if (.not. associated(mnode)) then
        write(unit=stderr, fmt=fmt_a) 'File manifest is empty.'
    else
        write(unit=stderr, fmt=fmt_a) 'File manifest contains:'
        do while (associated(mnode))
            ict = ict + 1
            if (mnode%is_archivable) then
                act = act + 1
            end if
            if (mnode%is_generated) then
                gct = gct + 1
            end if
            if (associated(mnode, this%head)) then
                write(unit=stderr, fmt=fmt_a) 'Node ' // itoa(ict)      &
                    // ' is the head element'
            end if
            if (associated(mnode, this%tail)) then
                write(unit=stderr, fmt=fmt_a) 'Node ' // itoa(ict)      &
                    // ' is the tail element'
            end if
            call mnode%dump(ict)

            mnode => mnode%next
        end do
        write(unit=stderr, fmt=fmt_a) 'List contains ' // itoa(ict)     &
            // ' elements, ' // itoa(act) // ' archivable and '         &
            // itoa(gct) // ' generated'
    end if

    return
end subroutine manifest_t_dump

!> Write file manifest contents to file unit(s)
subroutine manifest_t_write_to_unit(this, archivable_unit,              &
    generated_unit)
    use iso_fortran_env, only: stderr => error_unit
    use m_format, only: fmt_a
    implicit none

    !> Object reference
    class(manifest_t), intent(inout) :: this

    !> I/O unit listing archivable files, optional.
    integer, intent(in), optional :: archivable_unit

    !> I/O unit listing generated files, optional.
    integer, intent(in), optional :: generated_unit

    type(manifest_node_t), pointer :: mnode

    logical :: write_to_gunit
    logical :: write_to_aunit

    continue

    write_to_gunit = present(generated_unit)
    write_to_aunit = present(archivable_unit)

    if (write_to_gunit .or. write_to_aunit) then
        mnode => this%head
        do while (associated(mnode))
            if (write_to_aunit .and. mnode%is_archivable) then
                write(unit=archivable_unit, fmt=fmt_a) mnode%file
            end if

            if (write_to_gunit .and. mnode%is_generated) then
                write(unit=generated_unit, fmt=fmt_a) mnode%file
            end if

            mnode => mnode%next
        end do
    end if

    return
end subroutine manifest_t_write_to_unit

!> Add file and status flags to manifest
subroutine manifest_t_add(this, filename, archivable, generated)
    use m_textio, only: munch
    implicit none

    !> Object reference
    class(manifest_t), intent(inout) :: this

    !> Filename
    character(len=*), intent(in) :: filename

    !> Archivable status. Optional, defaults to .true.
    logical, intent(in), optional :: archivable

    !> Generated status. Optional, defaults to .false.
    logical, intent(in), optional :: generated

    logical :: a_state
    logical :: g_state

    type(manifest_node_t), pointer :: mnode
    ! type(manifest_node_t), target :: mnode

    continue

    if (present(archivable)) then
        a_state = archivable
    else
        a_state = .true.
    end if

    if (present(generated)) then
        g_state = generated
    else
        g_state = .false.
    end if

    if (a_state .or. g_state) then
        allocate(mnode)
        mnode = new_manifest_node(munch(filename), a_state, g_state)
        if (associated(this%head)) then
            this%tail%next => mnode
        else
            allocate(this%head)
            allocate(this%tail)
            this%head => mnode
        end if
        this%tail => mnode
    end if

    return
end subroutine manifest_t_add

!> Write manifests to files based on scenario name
subroutine manifest_t_write_to_file(this, BASENAME)
    use iso_fortran_env, only: stderr => error_unit
    use m_textio, only: munch, itoa
    use m_format, only: fmt_a
    implicit none

    !> Object reference
    class(manifest_t), intent(inout) :: this

    !> Base scenario name
    character(len=*), intent(in) :: BASENAME

    character(len=:), allocatable :: afn
    character(len=:), allocatable :: gfn
    integer :: a_unit
    integer :: g_unit

    character(len=80) :: msg
    integer :: iostatus

    continue

    afn = munch(BASENAME) // '_archive_manifest.txt'
    gfn = munch(BASENAME) // '_output_manifest.txt'

    iostatus = 0
    msg = "OK"
    open(newunit=a_unit, file=afn, action='WRITE',                  &
        status='REPLACE', form='FORMATTED', iostat=iostatus,        &
        iomsg=msg)
    if (iostatus == 0) then
        iostatus = 0
        msg = "OK"
        open(newunit=g_unit, file=gfn, action='WRITE',              &
            status='REPLACE', form='FORMATTED', iostat=iostatus,    &
            iomsg=msg)
        if (iostatus == 0) then

            call this%write_to_unit(archivable_unit=a_unit,         &
                generated_unit=g_unit)
            close(g_unit)

        else
            write(unit=stderr, fmt=fmt_a) 'Cannot write to ' // gfn &
            // ', ' // itoa(iostatus) // ': ' // munch(msg)
        end if

        close(a_unit)
    else
        write(unit=stderr, fmt=fmt_a) 'Cannot write to ' // afn     &
            // ', ' // itoa(iostatus) // ': ' // munch(msg)
    end if

    return
end subroutine manifest_t_write_to_file

end module sofire2_manifest