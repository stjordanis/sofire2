!> @file sofire2_param.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Contains numerical precision, array sizes, and similar
!! modeling constants used by SOFIRE2
!!
!! @note All members are considered public
module sofire2_param
! Alternate method of setting WP to single-precision
!    use iso_fortran_env, only: WP => REAL32
! Alternate method of setting WP to double-precision
!    use iso_fortran_env, only: WP => REAL64
    implicit none

    !> Default real precision (kind). Mnemonic: WP = "working precision"
    ! ! Uncomment for single-precision (default)
    ! integer, parameter :: WP = kind(1.0E0)
    ! Uncomment for double-precision
    integer, parameter :: WP = kind(1.0D0)

    !> Default size of scenario input array (greater than or equal to
    !! number of individual input parameters)
    integer, parameter :: ND = 120

    !> Default number of cell wall heat sinks
    integer, parameter :: n_hs_wall = 4

    !> Default number of cell floor heat sinks
    integer, parameter :: n_hs_floor = 4

    !> Default number of sodium pool heat sinks
    integer, parameter :: n_hs_na_pool = 4

    !> Default number of heat conduction paths
    !!
    !! Note: for the static one-cell SOFIRE II topology,
    !! there are 13 heat conduction paths:
    !! (n_hs_wall - 1) + (n_hs_floor - 1) + (n_hs_na_pool - 1)
    !! + crust/pool + pool/floor + floor/ambient + wall/ambient
    integer, parameter :: n_qcond_path = 13

    !> Default number of heat convection paths
    !!
    !! Note: for the static one-cell SOFIRE II topology,
    !! there are 2 heat conduction paths: crust-to-gas and gas-to-wall-liner
    integer, parameter :: n_qconv_path = 2

    !> Default number of heat radiation paths
    !!
    !! Note: for the static one-cell SOFIRE II topology,
    !! there are 4 heat conduction paths:
    !! QRAD1 (floor liner to floor node 2),
    !! QRAD2 (wall liner to wall node 2),
    !! QRAD3 (crust to gas), and
    !! QROD (crust to wall liner)
    integer, parameter :: n_qrad_path = 4

    !###################################################################
    ! Model parameters (i.e. important physical or modeling constants
    ! that do not appear to have a reference to theory or experiment)
    !###################################################################

    !> Fraction of generated sodium oxides which are emitted as aerosols,
    !! \f$\us{\lbm}-\ce{Na}_{x}\ce{O}_{y}\f$ per \f$\us{\lbm}-\ce{Na}\f$
    real(kind=WP) :: faer_naox = 0.2696_WP

    !> Fraction of generated sodium oxides which are emitted as aerosols;
    !! derived from faer_naox assuming \f$x = 2\f$ and \f$y = 1\f$ in
    !! \f$\ce{Na}_{x}\ce{O}_{y}\f$, 100% \f$\ce{Na2O}\f$ production
    real(kind=WP) :: faer = 0.2_WP

    !> Minimum amount of sodium which can maintain combustion,
    !! \f$\US{0.1}{\lbm}\f$
    real(kind=WP) :: min_sodium_mass = 0.1_WP

    !> Use specific gas constant for dry air, full oxygen content in
    !! calculation of ideal gas properties for cell gas.
    !!
    !! Setting this to `.true.` preserves the original 1973 code
    !! behavior but leads to inaccuracies. For example, the initial gas
    !! mass is incorrect if the initial \f$\ce{O2}\f$ concentration
    !! (`CO`) substantially differs from 0.23 (23 wt-% or 20.9 vol-%);
    !! this affects the entire analysis. As the scenario progresses,
    !! pressure and temperature calculations will lose accuracy as gas
    !! composition changes with forced flow, leakage, and combustion.
    !! Physically, `RIN` should vary with the composition of the gas,
    !! however the original SOFIRE II cell gas model did not track
    !! individual gas species and could not compensate for changes in
    !! gas composition
    logical :: use_legacy_rgas = .true.

! contains
end module sofire2_param