!> @file m_output.f90
!! @author Bob Apthorpe
!! @copyright See LICENSE
!! @brief Routines for writing and formatting results
module m_output

    use sofire2_param, only: WP
    use iso_fortran_env, only: stdout => output_unit, stderr => error_unit

    implicit none

    private

    ! Basic procedural CSV file opener/closer
    public :: get_csv_file
    public :: get_csv_unit
    public :: close_csv_unit

    !> @class CSV output class
    type, public :: csv_writer
        !> CSV data file name
        character(len=64) :: filename = 'sofire2_results.csv'

        !> Output unit number
        integer :: unit = stdout
    contains
        procedure :: open_file => open_csv_file
        procedure :: write_header => write_csv_header
        procedure :: write_data => write_csv_data
        procedure :: close_file => close_csv_file
        final :: destroy_csv_writer
    end type csv_writer

contains

!> Derive CSV file name from case ID
character(len=80) function get_csv_file(case_id, scenario)          &
    result(csv_fn)
    use m_textio, only: munch, itoa

    implicit none

    !> Case ID. Starts at 0 and should monotonically increase for each
    !! case simulated)
    integer, intent(in) :: case_id

    !> Scenario name, optional. If not specified, 'sofire2' will be
    !! used as a default
    character(len=*), intent(in), optional :: scenario

    character(len=:), allocatable :: scenario_a

    continue

    if (present(scenario)) then
        scenario_a = munch(scenario)
    else
        scenario_a = 'sofire2'
    end if

    if (case_id <= 0) then
        ! Omit counter digit on output CSV file name for first case
        csv_fn = scenario_a // "_results.csv"
    else
        ! Add counter digit to output CSV file name for subsequent
        ! cases
        csv_fn = scenario_a // "_results_" // itoa(case_id) // ".csv"
    end if

    return
end function get_csv_file

!> Open CSV file for writing returning open unit number. On error, return stdout.
integer function get_csv_unit(filename) result(iunit)
    use m_textio, only: munch
    implicit none

! Arguments

    !> CSV file name
    character(len=*), intent(in) :: filename

! Local variables

    integer :: iostatus
    character(len=80) :: msg

! Formats

10  format('WARNING: Cannot write to ', A, ', sending CSV output ',     &
           'to unit ', I4, ': ', A)

    continue

    iostatus = 0
    msg = "OK"
    open(newunit=iunit, file=filename, action='WRITE',                  &
        status='REPLACE', form='FORMATTED', iostat=iostatus, iomsg=msg)
    if (iostatus /= 0) then
        write(stderr, "('Tried writing to unit ', I0, ' but got ', I0, ': ', A)") &
            iunit, iostatus, munch(msg)

        ! Fallback to stdout
        iunit = stdout
        write(stderr, 10) munch(filename), iunit, munch(msg)
    end if

    return
end function get_csv_unit

!> Close CSV file if unit is not stdout and is open; generic procedural version.
subroutine close_csv_unit(iunit)
    implicit none

! Arguments

    !> CSV file unit
    integer, intent(inout) :: iunit

    logical :: is_open

    continue

    if (iunit /= stdout) then
        inquire(unit=iunit, opened=is_open)
        if (is_open) then
            close(iunit)
        end if
        iunit = stdout
    end if

    return
end subroutine close_csv_unit

!> Open CSV file for writing. On error, fall back to writing to stdout.
subroutine open_csv_file(this)
    implicit none

! Arguments

    !> Object instance
    class(csv_writer), intent(inout) :: this

    continue

    this%unit = get_csv_unit(this%filename)

    return
end subroutine open_csv_file

!> Write header line to CSV file
subroutine write_csv_header(this)
    implicit none

! Arguments

    !> Object instance
    class(csv_writer), intent(in) :: this

! Formats

10  format('"T", "XM", "TS", "TGASC", "TF1", "TF2", "TF3", "TF4", ',    &
           '"TWC1", "TWC2", "TWC3", "TWC4", "QCONV1", "QCONV2", ',      &
           '"PGASC", "QROD", "QRAD3", "TS1", "SUM2", "OX", "RHOC", ',   &
           '"SUM1", "W", "F40", "C", "W1", "W2", "QRAD1", "QRAD2", ',   &
           '"OXR", "DTMIN1", "DTMIN2", "DT", "DTMIN", "TS2", "TS3",',   &
           ' "TS4"')
    continue

    write(unit=this%unit, fmt=10)

    return
end subroutine write_csv_header

!> Write data line to CSV file
subroutine write_csv_data(this,                                         &
                          T, XM, TS, TGASC, TF1, TF2, TF3, TF4, TWC1,   &
                          TWC2, TWC3, TWC4, QCONV1, QCONV2, PGASC,      &
                          QROD, QRAD3, TS1, SUM2, OX, RHOC, SUM1, W,    &
                          F40, C, W1, W2, QRAD1, QRAD2, OXR, DTMIN1,    &
                          DTMIN2, DT, DTMIN, TS2, TS3, TS4)
    implicit none

! Arguments

    !> Object instance
    class(csv_writer), intent(in) :: this

    !> Scenario time, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: T

    !> Sodium burning rate, \f$\us{\lbm\per\hour\per\square\foot}\f$
    real(kind=WP), intent(in) :: XM

    !> Sodium surface temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS

    !> Cell gas temperature, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TGASC

    !> Liner floor temperature, center of first (top) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF1

    !> Cell floor temperature, center of second node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF2

    !> Cell floor temperature, center of third node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF3

    !> Cell floor temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TF4

    !> Liner wall temperature, center of first (inside) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC1

    !> Cell wall temperature, center of second node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC2

    !> Cell wall temperature, center of third node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC3

    !> Cell wall temperature, center of fourth (outside) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TWC4

    !> Convective heat transfer rate from sodium to gas, \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QCONV1

    !> Convective heat transfer rate from gas to wall (liner), \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QCONV2

    !> Cell gas pressure, \f$\us{\lbf\per\square\foot}\f$
    real(kind=WP), intent(in) :: PGASC

    !> Radiative heat transfer rate from sodium pool to wall (liner), \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QROD

    !> Radiative heat transfer rate from sodium pool to gas, \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QRAD3

    !> Sodium pool temperature, center of first (top) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS1

    !> Mass of sodium burned, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: SUM2

    !> Mass of oxygen remaining in cell, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: OX

    !> Gas density, \f$\us{\lbm\per\cubic\foot}\f$
    real(kind=WP), intent(in) :: RHOC

    !> Mass of oxygen consumed, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: SUM1

    !> Mass of gas in cell, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: W

    !> Volumetric gas leakage rate, \f$\us{\cubic\foot\per\hour}\f$
    real(kind=WP), intent(in) :: F40

    !> Oxygen concentration corrected for leakage, weight percent
    real(kind=WP), intent(in) :: C

    !> Gas leakage from cell due to exhaust, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: W1

    !> Gas inflow/outflow mass from cell due to pressure, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: W2

    !> Radiative heat transfer rate from wall liner to wall node 2, \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QRAD1

    !> Radiative heat transfer rate from floor liner to floor node 2, \f$\us{\BTU\per\hour}\f$
    real(kind=WP), intent(in) :: QRAD2

    !> Mass of sodium oxide released to gas, \f$\us{\lbm}\f$
    real(kind=WP), intent(in) :: OXR

    !> Timestep based on gas mass, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: DTMIN1

    !> Timestep based on sodium pool surface, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: DTMIN2

    !> Minimum timestep, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: DT

    !> Stabilized timestep, \f$\si{\hour}\f$
    real(kind=WP), intent(in) :: DTMIN

    !> Sodium pool temperature, center of second node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS2

    !> Sodium pool temperature, center of third node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS3

    !> Sodium pool temperature, center of fourth (bottom) node, \f$\us{\rankine}\f$
    real(kind=WP), intent(in) :: TS4

! Formats

10  format(ES12.5, 36(',', ES12.5))

    continue

    write(unit=this%unit, fmt=10)                                       &
        T, XM, TS, TGASC, TF1, TF2, TF3, TF4, TWC1, TWC2, TWC3, TWC4,   &
        QCONV1, QCONV2, PGASC, QROD, QRAD3, TS1, SUM2, OX, RHOC, SUM1,  &
        W, F40, C, W1, W2, QRAD1, QRAD2, OXR, DTMIN1, DTMIN2, DT,       &
        DTMIN, TS2, TS3, TS4

    return
end subroutine write_csv_data

!> Close CSV file if unit is not stdout.
subroutine close_csv_file(this)
    implicit none

! Arguments

    !> Object instance
    class(csv_writer), intent(inout) :: this

    continue

    call close_csv_unit(this%unit)

    return
end subroutine close_csv_file

!> Destructor for csv_writer class
subroutine destroy_csv_writer(this)
    implicit none

! Arguments

    !> Object instance
    type(csv_writer), intent(inout) :: this

    continue

    call this%close_file()

    return
end subroutine destroy_csv_writer

end module m_output
