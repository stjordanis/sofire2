\chapter{Introduction}

SOFIRE~II is a simple program for transient modeling of heat and mass transport
resulting from postulated sodium fires in a single- or two-compartment geometry.
It uses a `lumped parameter' finite element model to simulate sodium combustion,
aerosol production, conductive, convective, and radiative heat transport to
predict how temperature, pressure, and atmospheric composition vary with time.

This program was originally developed by the Atomics International Division of
Rockwell Internation as part of the US Atomic Energy Commission's research into
liquid metal fast breeder reactor (LMFBR) safety in the late 1960s / early 1970s.
SOFIRE~II was used as the basis for several later fast reactor safety and sodium
fire modeling programs as detailed in ANL-ARC-250 ``Sodium Pool Fire Phenomena,
Sodium Pool Fire Modeling in SOFIRE~II, and SOFIRE~II Calculations for the
AFR-100'' \cite{Sienicki2012}.

This reference provides a critical review of SOFIRE~II, noting several errors
in the original code and in the model development and discusses later work which
addressed some of SOFIRE~II's deficiencies. Still, the code is rather
straightforward, verifiable, and easy to understand and modify.

Several of the major errors and deficiencies documented in \cite{Sienicki2012}
have been addressed and results from the recovered modernized code compare
well with ANL's unpublished revisions.

\section{Project Goals}

The initial goals of the SOFIRE~II recovery and modernization project are to:

\begin{itemize}
    \item Recover the original source code as published in AI-AEC-13055
          \cite{Beiriger1973} to a compilable runnable state on
          \texttt{x86\_64} hardware
    \item Update the source code to well-formed Fortran 2018,
    \item Benchmark the revised code
    \begin{itemize}
        \item Against previous code revisions, and
        \item Against experimental data
    \end{itemize}
    \item Reconstitute a sustainable cross-platform build/test/packaging
          process based on CMake/CTest/CPack (see \url{https://cmake.org/}),
    \item Address numerical problems in the original code (\textit{i.e.}
          loss of numerical precision leading to ``clipping''; see
          \cite{Sienicki2012} and page~C-13 of \cite{Beiriger1973}).
    \item Correct code errors as noted in \cite{Sienicki2012}
    \item Provide change control, documentation, and testing consistent
          with the expectations of safety analysis software meeting
          ASME~NQA-1 quality assurance standards \cite{NQA-1-2008}.
\end{itemize}

This project has already met many of these goals for the single-cell
version of SOFIRE~II. More information about the SOFIRE~II recovery
project and the code's status may be found at
\url{https://gitlab.com/apthorpe/sofire2}.

\chapter{Introduction from AI-AEC-13055 (1973)}

In the design of Liquid Metal Fast Breeder Reactor (LMFBR) containment
vessels, it is important to know the maximum pressure for which the
containment has to be designed. This pressure is the result of heat from
the reaction of spilled sodium with oxygen in the atmosphere above the
sodium and to the sensible heat contained in the spilled sodium.
This process is a transient one and the maximum pressure is a function
of the sodium burning rate, rate of heat generation, and oxygen consumption.
To describe the pressure-temperature history of the containment following
a postulated sodium spill, a theoretical model (SOFIRE~II Code) has been
developed to compute 1-cell and 2-cell pool fire cases.
The basic equations of the SOFIRE~II Code are not restricted to any
specific size of the sodium pool and its containment.
While experimental verification of the code was conducted in a 30-ft high,
10-ft-diameter vessel with pool fire areas of \num{6}ft$^2$,
studies conducted in a vessel 6-ft high and 2.5-ft in diameter, of pool
fires from \num{0.03} to \num{0.2}~ft$^2$ in area in various oxygen
atmospheres gave an average initial oxygen consumption rate of
\num{0.15}~lb-\ce{O2}~/~hr-ft$^2$-\%\ce{O2} which is comparable to the large vessel
value of \num{0.17}~lb-\ce{O2}~/~hr-ft$^2$-\%\ce{O2}. These test results, at sodium
temperatures about 1000~$^{\circ}$F, together with results of
small open air pool fires provided sufficient understanding of the sodium
burning process that SOFIRE~II can be anticipated to conservatively predict the
pressures resulting from potential LMFBR accidents.

SOFIRE~II is a digital computer code which solves a set of theoretical
equations describing the simultaneous process of heat and mass transfer
by the finite difference method\footnote{F. P. Beiriger,
\emph{Sofire II Engineering Code User's Manual} TI-707-14-011, 1969
(internal technical report, Atomics International)}. The physical system is
simulated by a nodal network in which the nodes are connected to each
other along a heat transfer path by admittances, equal to the reciprocal
of the thermal resistance. Each node has a thermal capacity equal to that
of the physical counterpart represented by the node. The temperature
assigned to or calculated by each node represents the temperature at the
centroid of the corresponding element in the physical system.

Two versions of SOFIRE~II have been written to simulate a pool fire in a
single containment volume (Figure~\ref{fig:intro_c1_geom}) and in an interconnected double cell
(Figure~\ref{fig:intro_c2_geom}). Both these versions contain options, to be described later,
which are applicable to specific engineering techniques for reducing the
consequences of sodium pool fires.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig1_one_cell_geometry}
    \end{center}
    \caption{One-cell geometry}
    \label{fig:intro_c1_geom}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig2_two_cell_geometry}
    \end{center}
    \caption{Two-cell geometry}
    \label{fig:intro_c2_geom}
\end{figure}