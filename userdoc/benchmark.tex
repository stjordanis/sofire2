\chapter{Experimental Verification of Model}

NOTE: This section has been reproduced verbatim from \cite{Beiriger1973}
and has not been confirmed to reflect the current state of the code.
Updated experimental benchmarks are pending.

A schematic diagram of the large test vessel (LTV) is provided in
Figure~\ref{fig:ltv_diagram} (from \cite{Johnson1973}). The configuration of the LTC for the two-cell experiment
is shown in Figure~\ref{fig:ltv_c2_diagram} (from \cite{AI-AEC-13035}).

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{large_sodium_fires_modeling_experiment_diagram_1}
    \end{center}
    \caption{General schematic and dimensions of the Large Test Vessel}
    \label{fig:ltv_diagram}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C2_LTV_schematic}
    \end{center}
    \caption{Two-cell experimental configuration of the Large Test Vessel}
    \label{fig:ltv_c2_diagram}
\end{figure}

\clearpage

\section{One-Cell}

The large test vessel (LTV) was used in a series of thermodynamic parameter
tests to study the effect of oxygen concentration on the system pressure,
sodium burning rates, and heat transfer rates. This vessel, described in
detail in \cite{Johnson:ANL7520_1}, has a diameter of \US{10}{\foot}, stands about \US{30}{\foot}~high, and
contains \US{2200}{\cubic\foot} of gas at standard conditions. In the lower section
of the vessel, a \US{6}{\square\foot} steel pan was installed on a spider off the
floor of the vessel. The pan was insulated with fire brick and mounted
below a feed line from an external sodium preheat tank. Thermocouples were
mounted in or on the sodium pool volume, steel pan, the pan insulation, gas
volume, and vessel walls.

The test conditions and some of the experimental data for three of the tests
are shown in Table~\ref{tbl:c1_test_conditions}. Since very little oxidation was
expected to occur in Test~6 (2\%~\ce{O2}), the burn pan for this test was made
of 100-mil (\US{0.1}{\inch}) steel so that the sodium temperature would remain above the freezing
point. As shown in the table, both Tests~5 and 6 were poured at \US{1100}{\fahrenheit},
but the resultant pool temperature just after the spill was \US{150}{\fahrenheit}
hotter for Test~6 than Test~5.

\begin{table}[htbp]
    \caption{One cell test conditions and data\label{tbl:c1_test_conditions}}
    \begin{center}
        \begin{tabular}{lrrr}
        \toprule
Parameters                                                & Test 4 & Test 5 & Test 6 \\
        \midrule
Introduced \ce{Na} temperature (\us{\fahrenheit})         & 1000  & 1100 &  1100 \\
Weight of \ce{Na} introduced (\us{\lbm})                  & 50    & 50   &  50   \\
\ce{Na} pool temperature after spill (\us{\fahrenheit})   & 560   & 650  &  800  \\
Initial vessel atmosphere (\%\ce{O2})                     & 21    & 9.25 &  2.0  \\
Initial vessel pressure (\us{\psig})                      & 3.0   & 3.2  &  3.6  \\
Burn pan thickness (\us{\inch})                           & 0.25  & 0.25 &  0.01 \\
\midrule
Maximum bulk \ce{Na} temperature (\us{\fahrenheit})       & \numrange{1300}{1400} & 890 & 800 \\
Oxygen consumed (\%\ce{O2})                               & 10.7  & 5.3  &  0.34 \\
Average gas temperature rise (\us{\fahrenheit})           & 87    & 49   &  13   \\
Peak pressure rise (\us{\psid})                           & 2.77  & 1.51 &  0.41 \\
Experimental oxide fraction of sodium peroxide \ce{Na2O2} & 0.39  & 0.78 &  -    \\
        \bottomrule
        \end{tabular}
    \end{center}
\end{table}

SOFIRE~II comparisons were made for each of the tests, using the initial
sodium and vessel conditions, the weights of materials in the burn pan and vessel,
and the characteristics of these materials. Test~4 comparisons are shown
in Figures~\ref{fig:c1v4_pressure} and \ref{fig:c1v4_burn_rate}. These
results are typical of the comparisons of other
tests. The computed pressures and sodium burning rate are in good agreement with
the test data. Experimental burning rates were calculated from the measured
\ce{O2} change, the sodium surface areas, and the estimated products of combustion.
Typical comparisons for Test~5, starting at 9.25~volume-\% oxygen,
are shown in Figures~\ref{fig:c1v5_pressure} and \ref{fig:c1v5_burn_rate}.
Again, the computed pressure data compares well with the experimental. The burning
rate comparison in Figure~\ref{fig:c1v6_pressure} indicates
that an apparent change from the formation of sodium monoxide during the eariy
stages of the fire to 100\% sodium peroxide occurs at $\approx\SI{30}{\minute}$
after the start of the fire. Such changes have been confirmed by the analyses
for sodium peroxide of the aerosol leaving the pool surface.

Model comparisons with Test~6 data indicate that the system pressure and
gas temperature increase could be attributed primarily to heat transfer from
the sodium pool by convection and by radiation to the gas. The observed system
pressure and sodium pool temperatures are best matched without taking
oxidation into account as shown in Figures~\ref{fig:c1v6_pressure} and
\ref{fig:c1v6_pool_temp}. This is reasonable since the heat generated by the
oxidation of sodium equivalent to the low oxygen consumption is small in
comparison with the latent heat of the spilled sodium.

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig3_test_4_pressure_benchmark}
    \end{center}
    \caption{Comparison between measured and computed pressures for Test~4 (1973)}
    \label{fig:c1v4_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig4_test_4_burn_rate_benchmark}
    \end{center}
    \caption{Comparison between measured and computed sodium burning rates for Test~4 (1973)}
    \label{fig:c1v4_burn_rate}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig5_test_5_pressure_benchmark}
    \end{center}
    \caption{Comparison between measured and computed pressures for Test~5 (1973)}
    \label{fig:c1v5_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig6_test_5_burn_rate_benchmark}
    \end{center}
    \caption{Comparison between measured and computed sodium burning rates for Test~5 (1973)}
    \label{fig:c1v5_burn_rate}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig7_test_6_pressure_benchmark}
    \end{center}
    \caption{Comparison of measured and computed pressures for Test~6 (1973)}
    \label{fig:c1v6_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig8_test_6_pool_temp_benchmark}
    \end{center}
    \caption{Comparison of measured and computed sodium pool temperatures for Test~6 (1973)}
    \label{fig:c1v6_pool_temp}
\end{figure}

\clearpage
\section{One-Cell - Revised}

Placeholder images have been created for the one-cell code benchmarks;
see Figures~\ref{fig:c1v4_new_pressure} through \ref{fig:c1v6_new_pool_temp}.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_1}
    \end{center}
    \caption{Comparison between measured and computed pressures for Test~4}
    \label{fig:c1v4_new_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_2}
    \end{center}
    \caption{Comparison between measured and computed sodium burning rates for Test~4}
    \label{fig:c1v4_new_burn_rate}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_3}
    \end{center}
    \caption{Comparison between measured and computed pressures for Test~5}
    \label{fig:c1v5_new_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_4}
    \end{center}
    \caption{Comparison between measured and computed sodium burning rates for Test~5}
    \label{fig:c1v5_new_burn_rate}
\end{figure}

\clearpage

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_5}
    \end{center}
    \caption{Comparison of measured and computed pressures for Test~6}
    \label{fig:c1v6_new_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{C1_benchmark_6}
    \end{center}
    \caption{Comparison of measured and computed sodium pool temperatures for Test~6}
    \label{fig:c1v6_new_pool_temp}
\end{figure}

\clearpage

\subsection{Input Files}

\begin{table}[htbp]
    \caption{One-cell benchmark Test 4 input\label{input:c1b4}}
\VerbatimInput[numbers=left, numbersep=3pt, stepnumber=5,
    fontsize=\small, frame=single, labelposition=topline, label=C1B4.INP]{C1B4.INP}
\end{table}

\begin{table}[htbp]
    \caption{One-cell benchmark Test 5 input\label{input:c1b5}}
\VerbatimInput[numbers=left, numbersep=3pt, stepnumber=5,
    fontsize=\small, frame=single, labelposition=topline, label=C1B5.INP]{C1B5.INP}
\end{table}

\begin{table}[htbp]
    \caption{One-cell benchmark Test 6 input\label{input:c1b6}}
\VerbatimInput[numbers=left, numbersep=3pt, stepnumber=5,
    fontsize=\small, frame=single, labelposition=topline, label=C1B6.INP]{C1B6.INP}
\end{table}

\clearpage

\section{Two-Cell}

A two-cell test configuration was installed in LTV by placing a \US{1}{\foot}-thick
partition with a \US{1}{\foot}-diameter opening in the vessel so that the upper volume
was \US{1500}{\cubic\foot} in capacity. The lower cell consisted of a \US{34}{\cubic\foot}
right cylinder mounted below the opening. A burn pan surrounded by insulation
was placed in the bottom of the lower cell. Table~\ref{tbl:c2_test_conditions}
contains the initial test conditions for Test~2.

\begin{table}[htbp]
    \caption{Two cell test conditions and data\label{tbl:c2_test_conditions}}
    \begin{center}
        \begin{tabular}{lr}
        \toprule
Parameters                                                & Test 2 \\
        \midrule
Introduced \ce{Na} temperature (\us{\fahrenheit})         &  ?1000 \\
Weight of \ce{Na} spilled (\us{\lbm})                     &  80    \\
\ce{Na} pool temperature after spill (\us{\fahrenheit})   &  700   \\
Initial vessel atmosphere (\%\ce{O2})                     &  21    \\
Initial vessel pressure (\us{\psig})                      &  5.0   \\
Burn pan thickness (\us{\inch})                           &  ?0.25 \\
\midrule
Maximum bulk \ce{Na} temperature (\us{\fahrenheit})       &  ?890  \\
Oxygen consumed (\%\ce{O2})                               &  ?5.3  \\
Average gas temperature rise (\us{\fahrenheit})           &  ?49   \\
Peak pressure rise (\us{\psid})                           &  ?1.51 \\
Experimental oxide fraction of sodium peroxide \ce{Na2O2} &  ?0.78 \\
        \bottomrule
        \end{tabular}
    \end{center}
\end{table}

The agreement between the SOFIRE~II calculations and the measurements
was good, as shown in Figures~\ref{fig:c2v_pressure} and \ref{fig:c2v_ox_fraction}.
Figure~\ref{fig:c2v_pressure} shows that the best fit to the
pressure history would be obtained by the assumption of the initial formation of
sodium monoxide, with a later transition to sodium peroxide, in the same manner
as Test~5. In the presence of excess sodium, data on the free energies of
formation of sodium monoxide (\ce{Na2O}) and sodium peroxide (\ce{Na2O2}) indicate that,
at the temperatures characteristic of these tests, the monoxide species will predominantly
be formed; therefore, in the initial phases of a test (\textit{i.e.} during the
spill time and perhaps a few minutes thereafter when the sodium surfaces are
free of oxides, the formation of the monoxide is the more likely occurrence. It
would be expected, however, that the ratio of monoxide to peroxide would change
as the surface of the sodium becomes less available,due to oxide formation,and
the burning process proceeds more by way of small amounts of sodium rising up
through the oxide layers by capillary action to the surface. Under these conditions,
the peroxide formation is expected to predominate on the basis of thermodynamic
considerations. One would expect, therefore, that the oxide composition
would change from nearly pure \ce{Na2O} on the sodium surface to largely \ce{Na2O2}
at the top of the oxide layer. This condition has, in fact, been observed: the
oxide composition varied from 100\% \ce{Na2O} at the bottom of the oxide layer to 20
or 30\% at the top. A modified version of SOFIRE~II, which includes a variable
stoichiometric combustion ratio and heat of formation, was prepared and gave
results which agree better with experimental data. However, the use of the
present version with the assumption of the formation of sodium monoxide would
predict pressures which are more conservative than would actually be measured.

The mass transfer model appears to be adequate, since the oxygen history
in the secondary cell agrees with the experimental data, as shown in
Figure~\ref{fig:c2v_ox_fraction}.

An inherent problem with the present version of SOFIRE~II, two-cell is the
assumption that all the sodium to be spilled is present at time zero and that the
pool is tranquil with no surface perturbations. In the two-cell tests, the sodium
is spilled over a finite time and the surface is not smooth. The effect can be
seen in Figure~\ref{fig:c2v_pressure} where a pressure peak is measured at 5~min, which, even with
the assumption of 100\% sodium monoxide formation, SOFIRE~II calculations do
not predict. The experimental behavior could be better simulated by increasing
the heat transfer coefficient computed by the code to simulate the rough pool
surface during the time required to spill the sodium so that more heat is transferred
to the gas, resulting in a higher initial pressure.

The fraction ($Y$) of sodium monoxide found in the aerosol and other components
of sodium monoxide-sodium peroxide residues ($(l-Y)$ is given in
Table~\ref{tbl:c1_test_conditions}) has to be converted to $k$, the fraction of
total consumed oxygen used in the formation of sodium monoxide. This latter ratio
is used to compute $S$ and $\Delta H$, which are necessary inputs in the model as
discussed in Section~\ref{s:theory_c1_burn_rate}.

The relationship between $Y$ and $k$ is shown below:

\begin{equation}\label{eq:Y_to_k}
    k = \frac{0.257 Y}{0.257 Y + 0.41 \left( 1 - Y \right)}
\end{equation}
\nomenclature{$Y$}{Fraction of sodium monoxide found in the aerosol}%

where $Y$ is the fraction of sodium monoxide in total of sodium monoxide and sodium
peroxide found after sodium pool fire.

If $Y$ is 0.5, $k$ = 0.386 and if $k$ = 0.5, $Y$ = 0.61. Indiscriminate use
of $Y$ instead of $k$ to compute $S$ and $\Delta H$ would result in errors of
less than 4\% in the computed values of $\Delta H$.

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig9_c2_test_2_pressure_benchmark}
    \end{center}
    \caption{Code and experimental data comparisons for LTV two-cell Test 2}
    \label{fig:c2v_pressure}
\end{figure}

\begin{figure}[htbp]
    \begin{center}
        \includegraphics[width=\textwidth,height=20cm, keepaspectratio=true]{fig10_c2_test_2_ox_fraction_benchmark}
    \end{center}
    \caption{Comparison of secondary cell oxygen concentration}
    \label{fig:c2v_ox_fraction}
\end{figure}
