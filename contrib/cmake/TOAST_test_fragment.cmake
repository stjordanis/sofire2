# This example downloads and builds TOAST and links the TOAST library
# and *.mod files to test executable source 
# <project root>/unittest/c1/c1_basic.f90 to create a test executable
# and target named ut_c1_basic. The recipe to retrieve and build TOAST
# is BuildTOAST.cmake, stored under <project root>/cmake which is added
# to CMAKE_MODULE_PATH
#
# To test, add this fragment to your top-level CMakeLists.txt file,
# copy BuildTOAST.cmake to <project root>/cmake/BuildTOAST.cmake, and
# copy <toast root>/examples/example1/example.F90 to
# <project root>/unittest/c1/c1_basic.f90

### Set up dependencies ###

# Append local CMake module directory
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# Enable CTest
include(CTest)
enable_testing()

# Allow external projects to be imported
include(ExternalProject)

# Retrieve and build TOAST
include(BuildTOAST)

### Build unit test executable ###

set(TEST_SRC_BASE "${CMAKE_CURRENT_SOURCE_DIR}/unittest")

set(TEST_NAME ut_c1_basic)
set(TEST_SOURCES "${TEST_SRC_BASE}/c1/c1_basic.f90")
set(TEST_FORTRAN_MODULE_DIR "${CMAKE_CURRENT_BINARY_DIR}/${TEST_NAME}_include")
file(MAKE_DIRECTORY "${TEST_FORTRAN_MODULE_DIR}")
add_executable(${TEST_NAME} ${TEST_SOURCES})
set_target_properties(${TEST_NAME}
    PROPERTIES
    OUTPUT_NAME ${TEST_NAME}
    DEPENDS TOAST
    Fortran_MODULE_DIRECTORY ${TEST_FORTRAN_MODULE_DIR}
    INCLUDE_DIRECTORIES ${TOAST_MODULE_DIR}
    LINK_DIRECTORIES ${TOAST_LIBRARY_DIR}
    LINK_LIBRARIES ${TOAST_LIB_NAME}
)

### Run unit tests ###

add_test(NAME ${TEST_NAME}
    COMMAND ${TEST_NAME}
    CONFIGURATIONS Debug Release ""
)
