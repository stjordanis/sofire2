# Based on https://hub.docker.com/r/rhub/fedora-gcc/dockerfile

FROM fedora:latest

## Set a default user. Available via runtime flag
RUN useradd docker

# Add unprivileged user
RUN dnf install -y \
    sudo

RUN dnf install -y glibc-langpack-en.x86_64

RUN echo "nonprivuser ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/nonprivuser \
    && chmod 0440 /etc/sudoers.d/nonprivuser
RUN useradd --no-log-init --home-dir /home/nonprivuser --create-home --shell /bin/bash nonprivuser

# Add dev tooling
RUN dnf install -y \
    unzip \
    make \
    tar \
    gcc \
    gcc-c++ \
    gcc-gfortran \
    cmake \
    rpm-build

# Add doc tooling
RUN dnf install -y \
    doxygen \
    graphviz \
    gnuplot

# Add transport tooling
RUN dnf install -y \
    ca-certificates \
    curl \
    subversion \
    git

# Add gratuitous tooling
RUN dnf install -y \
    findutils \
    which

# RUN dnf install -y valgrind

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8

# Last step: set container default user
USER nonprivuser
WORKDIR /home/nonprivuser