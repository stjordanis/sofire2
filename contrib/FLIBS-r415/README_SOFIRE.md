# Adding FLIBS Source Code Dependencies without Network Access or Subversion

This directory contains two source files from svn revision r415 of FLIBS
(http://flibs.sf.net); see Copyright and README for licensing and other
details about FLIBS.

In the case where network access is restricted or Subversion is not
available, these files can be used to build SOFIRE2.

## Instructions

If `svn` is not available, `cmake/BuildFLIBS.cmake` will set
`FLIBS_MULTILOG_SRC` and `FLIBS_COMMAND_ARGS_SRC` to the path of the
local copies of `m_multilog.f90` and `m_multilog.f90` respectively;
no further user intervention is required.

If `svn` is available but use of the local FLIBS source files is
preferred , modify `CMakeLists.txt` as follows:

Replace the BuildFLIBS include fragment

~~~
# Import logging and command line argument processing from FLIBS
# Use local files if FLIBS cannot be retrieved over the network
include(BuildFLIBS)
~~~

with

~~~
# Temporarily disable SUBVERSION_FOUND so BuildFLIBS uses local source files
set(SUBVERSION_FOUND_ORIG "${SUBVERSION_FOUND}")
unset(SUBVERSION_FOUND)
# Import logging and command line argument processing from FLIBS
# Use local files if FLIBS cannot be retrieved over the network
include(BuildFLIBS)
# Restore original value of SUBVERSION_FOUND
set(SUBVERSION_FOUND "${SUBVERSION_FOUND_ORIG}")
~~~

then run `cmake` according to the build documentation.

Ideally, this only inhibits `svn` use for `cmake/BuildFLIBS.cmake` and
does not affect any other build processes which may rely on Subversion.
This is important in cases where the SOFIRE2 project is incorporated as
a subproject of a parent CMake build, for example one that uses
`add_subdirectory()` to build SOFIRE2.